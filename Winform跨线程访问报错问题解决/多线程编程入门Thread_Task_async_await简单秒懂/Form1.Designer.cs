﻿
namespace 多线程编程入门Thread_Task_async_await简单秒懂
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSingleThread = new System.Windows.Forms.Button();
            this.btnThread = new System.Windows.Forms.Button();
            this.btnTask = new System.Windows.Forms.Button();
            this.btnWith = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnNon = new System.Windows.Forms.Button();
            this.btnTongshi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSingleThread
            // 
            this.btnSingleThread.Location = new System.Drawing.Point(103, 62);
            this.btnSingleThread.Name = "btnSingleThread";
            this.btnSingleThread.Size = new System.Drawing.Size(106, 42);
            this.btnSingleThread.TabIndex = 0;
            this.btnSingleThread.Text = "单线程做菜";
            this.btnSingleThread.UseVisualStyleBackColor = true;
            this.btnSingleThread.Click += new System.EventHandler(this.btnSingleThread_Click);
            // 
            // btnThread
            // 
            this.btnThread.Location = new System.Drawing.Point(103, 134);
            this.btnThread.Name = "btnThread";
            this.btnThread.Size = new System.Drawing.Size(106, 42);
            this.btnThread.TabIndex = 1;
            this.btnThread.Text = "Thread做菜";
            this.btnThread.UseVisualStyleBackColor = true;
            this.btnThread.Click += new System.EventHandler(this.btnThread_Click);
            // 
            // btnTask
            // 
            this.btnTask.Location = new System.Drawing.Point(103, 202);
            this.btnTask.Name = "btnTask";
            this.btnTask.Size = new System.Drawing.Size(106, 42);
            this.btnTask.TabIndex = 2;
            this.btnTask.Text = "Task做菜";
            this.btnTask.UseVisualStyleBackColor = true;
            this.btnTask.Click += new System.EventHandler(this.btnTask_Click);
            // 
            // btnWith
            // 
            this.btnWith.Location = new System.Drawing.Point(275, 62);
            this.btnWith.Name = "btnWith";
            this.btnWith.Size = new System.Drawing.Size(106, 42);
            this.btnWith.TabIndex = 3;
            this.btnWith.Text = "同时做多道菜";
            this.btnWith.UseVisualStyleBackColor = true;
            this.btnWith.Click += new System.EventHandler(this.btnWith_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(274, 202);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(106, 42);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "一个执行完执行下一个";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnNon
            // 
            this.btnNon.Location = new System.Drawing.Point(274, 134);
            this.btnNon.Name = "btnNon";
            this.btnNon.Size = new System.Drawing.Size(106, 42);
            this.btnNon.TabIndex = 5;
            this.btnNon.Text = "无序执行";
            this.btnNon.UseVisualStyleBackColor = true;
            this.btnNon.Click += new System.EventHandler(this.btnNon_Click);
            // 
            // btnTongshi
            // 
            this.btnTongshi.Location = new System.Drawing.Point(441, 62);
            this.btnTongshi.Name = "btnTongshi";
            this.btnTongshi.Size = new System.Drawing.Size(106, 42);
            this.btnTongshi.TabIndex = 6;
            this.btnTongshi.Text = "两线程并行处理完后再执行后续";
            this.btnTongshi.UseVisualStyleBackColor = true;
            this.btnTongshi.Click += new System.EventHandler(this.btnTongshi_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 364);
            this.Controls.Add(this.btnTongshi);
            this.Controls.Add(this.btnNon);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnWith);
            this.Controls.Add(this.btnTask);
            this.Controls.Add(this.btnThread);
            this.Controls.Add(this.btnSingleThread);
            this.Name = "Form1";
            this.Text = "多线程基础";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSingleThread;
        private System.Windows.Forms.Button btnThread;
        private System.Windows.Forms.Button btnTask;
        private System.Windows.Forms.Button btnWith;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnNon;
        private System.Windows.Forms.Button btnTongshi;
    }
}

