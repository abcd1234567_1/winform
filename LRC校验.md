```
        /**
         *   纵向冗余校验（Longitudinal Redundancy Check，简称：LRC）是通信中常用的一种校验形式，也称LRC校验或纵向校验。
         *   它是一种从纵向通道上的特定比特串产生校验比特的错误检测方法。在行列格式中（如磁带），LRC经常是与VRC一起使用，这样就会为每个字符校验码。
         *   在工业领域Modbus协议Ascii模式采用该算法。 LRC计算校验码,具体算法如下：
        /// 1、对需要校验的数据（2n个字符）两两组成一个16进制的数值求和。
        /// 2、将求和结果与256求模。
        /// 3、用256减去所得模值得到校验结果（另一种方法：将模值按位取反然后加1）。
         */
```

单字节:  
```csharp
        public static byte LRC(byte[] auchMsg)
        {
            byte uchLRC = 0;
            foreach (byte item in auchMsg)
            {
                uchLRC += item;
            }
            return (byte)((uchLRC ^ 0xFF) + 1);
        }
```

双字节:  
```csharp
        public static ushort LRC(byte[] auchMsg)
        {
            ushort uchLRC = 0;

            foreach (byte item in auchMsg)
            {
                uchLRC += item;
            }

            return (ushort)((uchLRC ^ 0xFFFF) + 1);
        }
```

双字节返回字节数组:  
```csharp
        public static byte[] LRC(byte[] auchMsg)
        {
            ushort uchLRC = 0;

            foreach (byte item in auchMsg)
            {
                uchLRC += item;
            }

            ushort us = (ushort)((uchLRC ^ 0xFFFF) + 1);

            byte b1 = (byte)(us / 256);
            byte b2 = (byte)(us % 256);

            return new byte[] { b1, b2 };
        }
```