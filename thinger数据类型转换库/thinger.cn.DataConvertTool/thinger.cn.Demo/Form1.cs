﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using thinger.cn.DataConvertHelper;

namespace thinger.cn.Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Convert_Click(object sender, EventArgs e)
        {
            //自定义截取字节数组


            //byte[] src = new byte[] { 0x01, 0x32, 0x33 };

            //byte[] res = ByteArrayLib.GetByteArray(src, 1, 2);

            //MessageBox.Show(StringLib.GetHexStringFromByteArray(res,0,2,'|'));


            //16进制字符串转换成字节数组   "01 02 03 04"

            //string str = "1234";

            //byte[] res = ByteArrayLib.GetByteArrayFromHexStringWithoutSpilt(str);

            //MessageBox.Show(StringLib.GetHexStringFromByteArray(res,0,2,'|'));



            //布尔数组转换成字节数组
            //bool[] src = new bool[] { true, false, true, false, true, false, true, false, true, false };

            //byte[] res = ByteArrayLib.GetByteArrayFromBoolArray(src);

            //MessageBox.Show(StringLib.GetHexStringFromByteArray(res, 0, res.Length, '|'));



            //byte[] src1 = new byte[] { 0x01, 0x32, 0x33 };
            //byte[] src2 = new byte[] { 01, 32, 33 };




            //byte[] src = new byte[] { 0x01, 0x32, 0x33, 0x44, 0x01, 0x32, 0x33, 0x44, 0x01, 0x32, 0x33, 0x44 };
            //默认43是int类型,所以会替换src字节数组，从2开始后的4个字节
            //byte[] res = ByteArrayLib.SetByteArray(src, 43, 2, 0);
            //假设希望是short类型,需要强转
            //byte[] res = ByteArrayLib.SetByteArray(src, (short)43, 2, 0);
            //byte[] res = ByteArrayLib.SetByteArray(src, (float)43.332, 2, 0);
            //byte[] zzz = ByteArrayLib.GetByteArrayFromFloat((float)43.332);
            //byte[] temp = ByteArrayLib.GetByteArrayFromFloat((float)43.332,DataFormat.DCBA);
            //MessageBox.Show(StringLib.GetHexStringFromByteArray(res, 0, res.Length, '|'));


            //1000   0.1  10   y=kx+b     110

            //2000


            //一个方法要返回很多个参数：
            //对象
            //集合 数组
            //ref out

            //使用XktResult


            //var result = Test3();

            //if (result.IsSuccess)
            //{
            //    MessageBox.Show(result.Content2.ToString());
            //}



            //var result = Test4("324");

            //if (result.IsSuccess)
            //{
            //    MessageBox.Show(result.Message + ":" + result.Content);
            //}
            //else
            //{
            //    MessageBox.Show(result.Message);
            //}



            //读取的是什么？  字节数组

            //拿到的都是一样的  字节数组

            //S7通信


            //objS7.Connect("192.168.1.60", xktComm.CPU_Type.S7200SMART, 0, 0);

            //var res1 = objS7.ReadBytes(xktComm.StoreType.DataBlock, 1, 0, 100);

            //objModbus.Connect("192.168.1.60", "502");

            //var res2 = objModbus.ReadKeepReg(0, 50);

            //Thread.Sleep(10);

            //________________________________________________


            //解析数据

            //VW88
            //short res = ShortLib.GetShortFromByteArray(res1, 88);

            //V88.5 
            //bool b1 = BitLib.GetBitFrom2Byte(ByteArrayLib.Get2ByteArray(res1, 88), 5,true);

            //VD0  Float
            //float f1 = FloatLib.GetFloatFromByteArray(res1, 0);

            //MessageBox.Show(b1.ToString());



        }

        private xktComm.SiemensS7 objS7 = new xktComm.SiemensS7();

        private xktComm.ModbusTcp objModbus = new xktComm.ModbusTcp();



        private XktResult Test()
        {
            XktResult res = new XktResult();
            res.IsSuccess = true;
            res.Message = "转换失败";
            res.ErrorCode = 1000;
            return res;
        }


        private XktResult<short> Test2()
        {
            XktResult<short> res = new XktResult<short>();
            res.Content = 123;
            res.IsSuccess = true;
            res.Message = "转换失败";
            res.ErrorCode = 1000;
            return res;
        }

        private XktResult<short, short> Test3()
        {
            XktResult<short, short> res = new XktResult<short, short>();
            res.Content1 = 123;
            res.Content2 = 456;
            res.IsSuccess = true;
            res.Message = "转换失败";
            res.ErrorCode = 1000;
            return res;
        }

        private XktResult<short> Test4(string val)
        {
            XktResult<short> res = new XktResult<short>();

            short result = 0;
            if (short.TryParse(val, out result))
            {
                res.Content = result;
                res.IsSuccess = true;
                res.Message = "转换成功";
                res.ErrorCode = 0;
            }
            else
            {
                res.Content = result;
                res.IsSuccess = false;
                res.Message = "转换失败";
                res.ErrorCode = 1000;
            }
            return res;
        }

    }
}
