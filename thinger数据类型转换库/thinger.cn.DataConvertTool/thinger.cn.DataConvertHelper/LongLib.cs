using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


// ***********************************************************************
//    Assembly       : 新阁教育
//    Created          : 2020-11-11
// ***********************************************************************
//     Copyright by 新阁教育（天津星阁教育科技有限公司）
//     QQ：        2934008828（付老师）  
//     WeChat：thinger002（付老师）
//     公众号：   dotNet工控上位机
//     哔哩哔哩：dotNet工控上位机
//     知乎：      dotNet工控上位机
//     头条：      dotNet工控上位机
//     视频号：   dotNet工控上位机
//     版权所有，严禁传播
// ***********************************************************************

namespace thinger.cn.DataConvertHelper
{
    /// <summary>
    /// Long转换类
    /// </summary>
    public class LongLib
    {

        #region 字节数组中截取转成64位整型
        /// <summary>
        /// 字节数组中截取转成64位整型
        /// </summary>
        /// <param name="source"></param>
        /// <param name="start"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static long GetLongFromByteArray(byte[] source, int start=0, DataFormat type = DataFormat.ABCD)
        {
            byte[] b = ByteArrayLib.Get8ByteArray(source, start, type);
            return b == null ? 0 : BitConverter.ToInt64(b, 0);
        }
        #endregion

        #region 将字节数组中截取转成64位整型数组
        /// <summary>
        /// 将字节数组中截取转成64位整型数组
        /// </summary>
        /// <param name="source"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static long[] GetLongArrayFromByteArray(byte[] source, DataFormat type = DataFormat.ABCD)
        {
            long[] values = new long[source.Length / 8];

            for (int i = 0; i < source.Length / 8; i++)
            {
                values[i] = GetLongFromByteArray(source, 8 * i, type);
            }

            return values;
        }
        #endregion

        #region 将字符串转转成64位整型数组
        /// <summary>
        /// 将字符串转转成64位整型数组
        /// </summary>
        /// <param name="val"></param>
        /// <param name="spilt"></param>
        /// <returns></returns>
        public static long[] GetLongArrayFromString(string val, char spilt = ' ')
        {
            val = val.Trim();

            List<long> Result = new List<long>();

            if (val.Contains(spilt))
            {
                string[] str = val.Split(new char[] { spilt }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in str)
                {
                    Result.Add(Convert.ToInt64(item.Trim()));
                }
            }
            else
            {
                Result.Add(Convert.ToInt64(val.Trim()));
            }

            return Result.ToArray();
        }
        #endregion

    }
}
