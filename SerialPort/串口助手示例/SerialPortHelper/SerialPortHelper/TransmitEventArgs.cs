﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace SerialPortHelper
{
    //委托
    public delegate void TransmitData(byte[] data);

    //基于事件的委托
    public delegate void TransmitEventHandler(object sender, TransmitEventArgs e);

    /// <summary>
    /// 事件可以传递各种参数进去,我们可以构造参数的一个结构
    /// </summary>
    public class TransmitEventArgs : EventArgs
    {
        /// <summary>
        /// SerialPort作为参数传递
        /// </summary>
        //SerialPort sp { get; set; }

        public byte[] data { get; set; }
    }
}
