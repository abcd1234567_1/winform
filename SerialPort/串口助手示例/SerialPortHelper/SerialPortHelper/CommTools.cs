﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialPortHelper
{
    /// <summary>
    /// 公共工具类
    /// </summary>
    public static class CommTools
    {
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <param name="frameBuffer"></param>
        /// <returns></returns>
        public static bool crc_check(byte[] frameBuffer)
        {
            bool ret = false;

            //获取CRC之前的数据
            byte[] temp = new byte[frameBuffer.Length - 2];
            Array.Copy(frameBuffer, 0, temp, 0, temp.Length);
            //采用大端模式:DataCheck.BigOrLittle.BigEndian
            byte[] crcdata = DataCheck.DataCrc16_Ccitt(temp, DataCheck.BigOrLittle.BigEndian);

            if (crcdata[0] == frameBuffer[frameBuffer.Length - 2] && crcdata[1] == frameBuffer[frameBuffer.Length - 1])
            {
                //校验成功 check ok
                ret = true;
            }

            return ret;
        }
    }
}
