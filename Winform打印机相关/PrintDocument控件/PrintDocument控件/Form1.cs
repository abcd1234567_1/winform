﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace PrintDocument控件
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// PrintDocument：用于打印最重要的类，几乎所有的打印类都与这个类有关系。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // 1、打印页面设置
        private void btnSetting_Click(object sender, EventArgs e)
        {
            // 打印页面设置对话框
            PageSetupDialog page = new PageSetupDialog();

            page.Document = printDocument1;
            page.AllowMargins = true;
            page.AllowOrientation = true;
            page.AllowPaper = true;
            page.AllowPrinter = true;
            page.ShowHelp = true;


            if (page.ShowDialog() == DialogResult.OK)
            {
                // 将设置好的打印页 用作 PrintDocument进行打印。
                printDocument1.DefaultPageSettings = page.PageSettings;
            }
        }

        // 2、打印机设置
        private void btnPrint_Click(object sender, EventArgs e)
        {
            // 打印机设置对话框
            PrintDialog print = new PrintDialog();

            print.Document = printDocument1;
            print.AllowCurrentPage = true;
            print.AllowPrintToFile = true;
            print.AllowSelection = true;
            print.AllowSomePages = true;
            print.ShowHelp = true;
            if (print.ShowDialog() == DialogResult.OK)
            {
                // 将设置好的打印机 用作 PrinDocument进行打印。
                printDocument1.PrinterSettings = print.PrinterSettings;
            }
        }

        // 3、打印预览
        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否要预览打印文件？", "打印预览", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                // 设置要预览的文档
                printPreviewDialog1.Document = printDocument1;
                // 开启操作系统的抗锯齿功能
                printPreviewDialog1.UseAntiAlias = true;

                // 打开预览窗口
                if (printPreviewDialog1.ShowDialog() == DialogResult.OK)
                {
                    // 如果选择的是系统默认打印机，点击“打印”按钮之后，会跳出“文件另存为”窗口；
                    // 如果选择别的打印机，点击“打印”之后，会直接打印，不会返回“OK”。
                    MessageBox.Show("开始打印");
                }
                else
                {
                    MessageBox.Show("关闭预览");
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // PrintController：控制一个PrintDocument是如何打印的。
            PrintController printController = new StandardPrintController();
            printDocument1.PrintController = printController;
            printDocument1.DocumentName = "社保样卡";
            printDocument1.PrinterSettings.PrinterName = "Foxit PDF Reader Printer";
            printDocument1.Print(); // 触发Print_Page事件。

        }

        // PrintDocument 三个事件中的第二个参数 e 有如下属性：
        // e.Cancel：设置为true，将取消这次打印作业。
        // e.Griphics：所使用打印机的设备环境。
        // e.HasMorePages：PrintPage事件打印一页后，如果仍有数据未打印，退出事件前设置
        // HasMorePages=true；退出事件之后将再次出发PrintPage事件，打印下一页。
        // e.MarginBounds：打印区域的大小，是Rectangle结构，元素包括左上角坐标（Left和Top），
        //                 宽和高(Width和Height)，单位为1/100英寸。
        // e.PageSettings：PageSettings类对象，包含用对话框PageSetupDialog设置的页面打印方式的
        //                全部信息，
 
        // 在调用 Print 方法后，在打印文档的第一页之前发生。
        private void printDocument1_BeginPrint(object sender, PrintEventArgs e)
        {

        }

        // 需要打印新的一页时发生，负责打印一页所需要的数据
        // 打印预览会调用该事件，预览的内容就是此处设置好的内容。
        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            Image iamge = Image.FromFile(@"D:\photo\test.png");

            e.Graphics.DrawString("社保卡样卡", new Font("黑体", 35), Brushes.Black, new Point(300, 120));

            e.Graphics.DrawString("姓名 张三", new Font("黑体", 25), Brushes.Black, new Point(200, 270));
            e.Graphics.DrawString("社会保障号码 32032032302030230", new Font("黑体", 25), Brushes.Black, new Point(200, 360));
            e.Graphics.DrawString("社会保障卡号 JS2018098", new Font("黑体", 25), Brushes.Black, new Point(200, 450));
            e.Graphics.DrawString("制卡日期 2016年5月", new Font("黑体", 25), Brushes.Black, new Point(200, 540));
            e.Graphics.DrawImage(iamge, new Point(200, 190));
        }

        // 在打印完最后一页文档时发生。
        private void printDocument1_EndPrint(object sender, PrintEventArgs e)
        {
          

        }
        /// <summary>
        /// 打印机状态值
        /// </summary>
        public enum PrinterStatus
        {
            其他状态 = 1,
            未知,
            空闲,
            正在打印,
            预热,
            停止打印,
            打印中,
            离线
        }

        // 获取指定打印机的状态
        private void btnState_Click(object sender, EventArgs e)
        {
            //打印设置里找了一个打印机:Foxit PDF Reader Printer
            string strPrinter = "win32_printer.DeviceId='Foxit PDF Reader Printer'";

            // 用指定的打印机实例化一个打印机对象。
            ManagementObject printer = new ManagementObject(strPrinter);

            // 获取打印机信息。
            printer.Get();

            // 获取打印机状态属性
            string str = printer.Properties["PrinterStatus"].Value.ToString();
            textBox1.Text = str;

        }

        // 获取本地所有打印机信息
        private void btnAllPrint_Click(object sender, EventArgs e)
        {
            string strPrinter = "win32_printer";

            // 获取本地所有打印机
            ManagementClass mc = new ManagementClass(strPrinter);

            // 获取所有打印机实例的集合
            ManagementObjectCollection moc = mc.GetInstances();

            // 遍历本地所有打印机
            foreach (ManagementObject printer in moc)
            {
                // 打印机名字
                string strName = printer.Properties["DeviceId"].Value.ToString();
                // 打印机状态
                string strState = printer.Properties["PrinterStatus"].Value.ToString();
                comboBox1.Items.Add(strName);
            }
        }
    }
}
