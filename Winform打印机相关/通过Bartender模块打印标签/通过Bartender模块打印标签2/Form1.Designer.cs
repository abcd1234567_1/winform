﻿namespace 通过Bartender模块打印标签2
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_model = new System.Windows.Forms.TextBox();
            this.listb_models = new System.Windows.Forms.ListBox();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_sex = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_class = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nup_num = new System.Windows.Forms.NumericUpDown();
            this.btn_print = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nup_num)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "模板名:";
            // 
            // tb_model
            // 
            this.tb_model.Location = new System.Drawing.Point(63, 23);
            this.tb_model.Name = "tb_model";
            this.tb_model.Size = new System.Drawing.Size(308, 21);
            this.tb_model.TabIndex = 1;
            this.tb_model.TextChanged += new System.EventHandler(this.tb_model_TextChanged);
            // 
            // listb_models
            // 
            this.listb_models.FormattingEnabled = true;
            this.listb_models.ItemHeight = 12;
            this.listb_models.Location = new System.Drawing.Point(12, 67);
            this.listb_models.Name = "listb_models";
            this.listb_models.Size = new System.Drawing.Size(359, 364);
            this.listb_models.TabIndex = 2;
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(481, 135);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(114, 21);
            this.tb_name.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(440, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "姓名:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(440, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "班级:";
            // 
            // tb_sex
            // 
            this.tb_sex.Location = new System.Drawing.Point(481, 171);
            this.tb_sex.Name = "tb_sex";
            this.tb_sex.Size = new System.Drawing.Size(114, 21);
            this.tb_sex.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(440, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "性别:";
            // 
            // tb_class
            // 
            this.tb_class.Location = new System.Drawing.Point(481, 210);
            this.tb_class.Name = "tb_class";
            this.tb_class.Size = new System.Drawing.Size(114, 21);
            this.tb_class.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(440, 277);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "数量:";
            // 
            // nup_num
            // 
            this.nup_num.Location = new System.Drawing.Point(479, 275);
            this.nup_num.Name = "nup_num";
            this.nup_num.Size = new System.Drawing.Size(120, 21);
            this.nup_num.TabIndex = 10;
            this.nup_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nup_num.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btn_print
            // 
            this.btn_print.Location = new System.Drawing.Point(479, 328);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(116, 54);
            this.btn_print.TabIndex = 11;
            this.btn_print.Text = "打印";
            this.btn_print.UseVisualStyleBackColor = true;
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 450);
            this.Controls.Add(this.btn_print);
            this.Controls.Add(this.nup_num);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_class);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_sex);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_name);
            this.Controls.Add(this.listb_models);
            this.Controls.Add(this.tb_model);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nup_num)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_model;
        private System.Windows.Forms.ListBox listb_models;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_sex;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_class;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nup_num;
        private System.Windows.Forms.Button btn_print;
    }
}

