﻿using Seagull.BarTender.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 通过Bartender模块打印标签2
{
    public partial class Form1 : Form
    {
        public Engine engine = new Engine();//打印机 引擎 

        public LabelFormatDocument format = null;//获取 模板内容

        #region 获取当前执行路径的七种方法

        //获取模块的完整路径

        string path1 = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;

        //获取和设置当前目录(从进程中启动的目录)的完全限定目录

        string path2 = System.Environment.CurrentDirectory;

        //获取应用程序的当前工作目录

        string path3 = System.IO.Directory.GetCurrentDirectory();

        //获取程序的基目录

        string path4 = System.AppDomain.CurrentDomain.BaseDirectory;

        //获取和设置包括该应用程序的目录的名称

        string path5 = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

        //获取启动了应用程序的可执行文件的路径

        string path6 = System.Windows.Forms.Application.StartupPath;

        //获取启动了应用程序的可执行文件的路径及文件

        string path7 = System.Windows.Forms.Application.ExecutablePath;

        #endregion

        public static string path = Application.StartupPath + @"\Model";//模板路径



        public static DirectoryInfo direct = new DirectoryInfo(path); // 实例化指定文件路径的目录

        /// <summary>
        /// 获取指定路径上的所有文件
        /// </summary>
        public FileInfo[] fileInfo = direct.GetFiles();

        /// <summary>
        /// 所有 模板文件 数据
        /// </summary>
        public List<string> filesList = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 加载模板文件名称 至 listBox控件
        /// </summary>
        public void loadList_Model()
        {
            //遍历所有文件
            foreach (var item in fileInfo)
            {

                if (item.Extension.ToUpper() == ".BTW")//筛选出指定格式的文件
                {
                    filesList.Add(item.Name);//填充

                }

            }

            listb_models.DataSource = filesList; //数据绑定
        }

        /// <summary>
        /// 执行打印
        /// </summary>
        /// <param name="printmodel">模板名</param>
        /// <param name="printnum">打印数量</param>
        /// <param name="Sname">姓名</param>
        /// <param name="Sex">性别</param>
        /// <param name="Sclass">班级</param>
        public void Pint_model(string printmodel, int printnum, string Sname, string Sex, string Sclass)
        {
            engine.Start();

            for (int i = 0; i < printnum; i++)
            {
                btn_print.Enabled = false;

                format = engine.Documents.Open(path + $"\\{printmodel}");

                if (Sname != "")
                {

                    format.SubStrings["姓名"].Value = Sname;
                    format.SubStrings["性别"].Value = Sex;
                    format.SubStrings["班级"].Value = Sclass;
                }

                Result rel = format.Print();//获取打印状态

                if (rel == Result.Success)
                {
                    MessageBox.Show("打印成功!");
                }
                else
                {
                    MessageBox.Show("打印失败!");
                }

            }

            btn_print.Enabled = true;

            engine.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadList_Model();//ListBox 模板数据加载
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            int num = (int)nup_num.Value;//打印数量

            string modelname;

            if (listb_models.SelectedIndex >= 0)
            {
                modelname = listb_models.SelectedItem.ToString();// 获取ListBox中选中的模板
                Pint_model(modelname, num, tb_name.Text, tb_sex.Text, tb_class.Text);
            }
            else
            {
                MessageBox.Show("请选择模板");
            }
        }

        //查询文本 文本改变
        private void tb_model_TextChanged(object sender, EventArgs e)
        {
            string searchTxt = tb_model.Text;// 要查询的信息

            List<string> searchModel = new List<string>(); //存放 查询出的文件名

            if (tb_model.Text.Trim().Length == 0)//如果查询框中的值为空

            {

                BindingSource bs = new BindingSource();

                bs.DataSource = filesList;//直接加载全部的文件数据

                listb_models.DataSource = bs;
            }
            else
            {
                for (int i = 0; i < filesList.Count; i++)
                {
                    searchModel = filesList.Where(m => m.Contains(searchTxt)).ToList();//筛选查找这个
                }

                BindingSource bs = new BindingSource();//新建数据源

                bs.DataSource = searchModel;//数据源 绑定 list数据

                listb_models.DataSource = bs;//ListBox控件重新绑定数据
            }
        }
    }
}
