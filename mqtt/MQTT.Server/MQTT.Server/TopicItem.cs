﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MQTT.Server
{
    public class TopicItem
    {
        public string Topic { get; set; }

        public int Count { get; set; }

        public List<string> Clients { get; set; } = new List<string>();
    }
}
