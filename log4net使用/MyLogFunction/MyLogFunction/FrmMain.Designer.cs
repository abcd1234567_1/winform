﻿namespace MyLogFunction
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.lst_Log = new System.Windows.Forms.ListView();
            this.时间日期 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.信息 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Error = new System.Windows.Forms.Button();
            this.btn_Info = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lst_Log
            // 
            this.lst_Log.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.时间日期,
            this.信息});
            this.lst_Log.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lst_Log.HideSelection = false;
            this.lst_Log.Location = new System.Drawing.Point(9, 59);
            this.lst_Log.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lst_Log.Name = "lst_Log";
            this.lst_Log.Size = new System.Drawing.Size(444, 292);
            this.lst_Log.SmallImageList = this.imageList1;
            this.lst_Log.TabIndex = 0;
            this.lst_Log.UseCompatibleStateImageBehavior = false;
            this.lst_Log.View = System.Windows.Forms.View.Details;
            this.lst_Log.DoubleClick += new System.EventHandler(this.lst_Log_DoubleClick);
            // 
            // 时间日期
            // 
            this.时间日期.Text = "时间日期";
            this.时间日期.Width = 200;
            // 
            // 信息
            // 
            this.信息.Text = "信息";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "error.png");
            this.imageList1.Images.SetKeyName(1, "info.png");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "日志显示：";
            // 
            // btn_Error
            // 
            this.btn_Error.BackColor = System.Drawing.Color.Red;
            this.btn_Error.FlatAppearance.BorderSize = 0;
            this.btn_Error.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Error.ForeColor = System.Drawing.Color.White;
            this.btn_Error.Location = new System.Drawing.Point(320, 22);
            this.btn_Error.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_Error.Name = "btn_Error";
            this.btn_Error.Size = new System.Drawing.Size(68, 28);
            this.btn_Error.TabIndex = 2;
            this.btn_Error.Text = "错误信息";
            this.btn_Error.UseVisualStyleBackColor = false;
            this.btn_Error.Click += new System.EventHandler(this.btn_Error_Click);
            // 
            // btn_Info
            // 
            this.btn_Info.BackColor = System.Drawing.Color.Green;
            this.btn_Info.FlatAppearance.BorderSize = 0;
            this.btn_Info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Info.ForeColor = System.Drawing.Color.White;
            this.btn_Info.Location = new System.Drawing.Point(207, 22);
            this.btn_Info.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_Info.Name = "btn_Info";
            this.btn_Info.Size = new System.Drawing.Size(68, 28);
            this.btn_Info.TabIndex = 2;
            this.btn_Info.Text = "正确信息";
            this.btn_Info.UseVisualStyleBackColor = false;
            this.btn_Info.Click += new System.EventHandler(this.btn_Info_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "【双击内容清空】";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 360);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_Info);
            this.Controls.Add(this.btn_Error);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lst_Log);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmMain";
            this.Text = "日志功能测试";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lst_Log;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Error;
        private System.Windows.Forms.ColumnHeader 时间日期;
        private System.Windows.Forms.ColumnHeader 信息;
        private System.Windows.Forms.Button btn_Info;
        private System.Windows.Forms.Label label2;
    }
}

