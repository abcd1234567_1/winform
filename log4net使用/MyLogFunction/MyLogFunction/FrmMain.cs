﻿using Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLogFunction
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();

            // ListViewItem:第二列的宽=ListView的宽 - 第一列的宽 - 20
            this.lst_Log.Columns[1].Width = this.lst_Log.Width - this.lst_Log.Columns[0].Width - 20;
        }

        public void AddLog(int index, string info, bool isInfo = true)
        {
            // 第二个参数是使用的图标索引
            ListViewItem listViewItem = new ListViewItem("  " + DateTime.Now.ToString("yyyy:MM:dd HH:mm:ss"), index);

            // 多线程添加日志
            if (this.lst_Log.InvokeRequired)
            {
                this.lst_Log.Invoke(new Action(() =>
                {
                    listViewItem.SubItems.Add(info);
                    this.lst_Log.Items.Insert(0, listViewItem);
                }));
            }
            else  // 非多线程添加日志
            {
                listViewItem.SubItems.Add(info);
                this.lst_Log.Items.Insert(0, listViewItem);
            }

            if (isInfo)
            {
                LogHelper.Info(info);
            }
            else
            {
                LogHelper.Error(info, new Exception());
            }



        }

        private void lst_Log_DoubleClick(object sender, EventArgs e)
        {
            lst_Log.Items.Clear();
        }

        private void btn_Info_Click(object sender, EventArgs e)
        {
            AddLog(1, "插入了一条正常数据");
        }

        private void btn_Error_Click(object sender, EventArgs e)
        {
            AddLog(0, "插入了一条报警数据",false);
        }
    }
}
