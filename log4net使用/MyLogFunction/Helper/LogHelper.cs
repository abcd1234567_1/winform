﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//需要加这行:[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = false)],否则日志无法写入文件
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = false)]
namespace Helper
{
    /// <summary>
    /// 打印日志 
    /// </summary>
    public class LogHelper
    {
        /// <summary>
        /// 普通日志
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Info(string message)
        {
            log4net.ILog log = log4net.LogManager.GetLogger("InfoLog");
            if (log.IsInfoEnabled)
            {
                log.Info(message);
            }
            log = null;
        }
        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="message">错误日志</param>
        public static void Error(string message, Exception ex)
        {
            log4net.ILog log = log4net.LogManager.GetLogger("Error");
            if (log.IsInfoEnabled)
            {
                log.Error(message, ex);
            }
            log = null;
        }
    }
}
