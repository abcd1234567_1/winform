﻿namespace _02_ModbusTcpClient_1._0._1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.BtnMin = new Sunny.UI.UIButton();
            this.BtnHelp = new Sunny.UI.UIButton();
            this.BtnClose = new Sunny.UI.UIButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TbxFunction = new Sunny.UI.UITextBox();
            this.TbxPort = new Sunny.UI.UITextBox();
            this.TbxStartAddress = new Sunny.UI.UITextBox();
            this.TbxUnit = new Sunny.UI.UITextBox();
            this.TbxCount = new Sunny.UI.UITextBox();
            this.TbxIpAddress = new Sunny.UI.UITextBox();
            this.BtnConnect = new Sunny.UI.UIButton();
            this.BtnDisconnect = new Sunny.UI.UIButton();
            this.BtnReadData = new Sunny.UI.UIButton();
            this.BtnWriteData = new Sunny.UI.UIButton();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.TbxMessage = new Sunny.UI.UITextBox();
            this.uiPanel2 = new Sunny.UI.UIPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CloseTimer = new System.Windows.Forms.Timer(this.components);
            this.uiPanel1.SuspendLayout();
            this.uiPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.BtnMin);
            this.uiPanel1.Controls.Add(this.BtnHelp);
            this.uiPanel1.Controls.Add(this.BtnClose);
            this.uiPanel1.Controls.Add(this.label7);
            this.uiPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(30)))), ((int)(((byte)(63)))));
            this.uiPanel1.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(30)))), ((int)(((byte)(63)))));
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanel1.ForeColor = System.Drawing.Color.White;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(58)))), ((int)(((byte)(92)))));
            this.uiPanel1.Size = new System.Drawing.Size(658, 34);
            this.uiPanel1.Style = Sunny.UI.UIStyle.DarkBlue;
            this.uiPanel1.TabIndex = 17;
            this.uiPanel1.Text = null;
            this.uiPanel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnMin
            // 
            this.BtnMin.BackColor = System.Drawing.Color.Black;
            this.BtnMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.BtnMin.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnMin.Location = new System.Drawing.Point(577, 0);
            this.BtnMin.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnMin.Name = "BtnMin";
            this.BtnMin.Size = new System.Drawing.Size(27, 34);
            this.BtnMin.Style = Sunny.UI.UIStyle.DarkBlue;
            this.BtnMin.TabIndex = 23;
            this.BtnMin.Text = "__";
            this.BtnMin.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnMin.Click += new System.EventHandler(this.BtnMin_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.BackColor = System.Drawing.Color.Black;
            this.BtnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnHelp.Dock = System.Windows.Forms.DockStyle.Right;
            this.BtnHelp.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnHelp.Location = new System.Drawing.Point(604, 0);
            this.BtnHelp.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(27, 34);
            this.BtnHelp.Style = Sunny.UI.UIStyle.DarkBlue;
            this.BtnHelp.TabIndex = 22;
            this.BtnHelp.Text = "❓";
            this.BtnHelp.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.BackColor = System.Drawing.Color.Black;
            this.BtnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.BtnClose.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnClose.Location = new System.Drawing.Point(631, 0);
            this.BtnClose.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(27, 34);
            this.BtnClose.Style = Sunny.UI.UIStyle.DarkBlue;
            this.BtnClose.TabIndex = 18;
            this.BtnClose.Text = "×";
            this.BtnClose.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(7, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 21);
            this.label7.TabIndex = 17;
            this.label7.Text = "ModbusTcp_Tools";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(67, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 27);
            this.label3.TabIndex = 2;
            this.label3.Text = "Unit:";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(66, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(10, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "IpAddress:";
            // 
            // TbxFunction
            // 
            this.TbxFunction.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxFunction.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxFunction.Location = new System.Drawing.Point(466, 25);
            this.TbxFunction.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxFunction.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxFunction.Name = "TbxFunction";
            this.TbxFunction.ShowText = false;
            this.TbxFunction.Size = new System.Drawing.Size(185, 29);
            this.TbxFunction.TabIndex = 14;
            this.TbxFunction.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TbxFunction.Watermark = "";
            // 
            // TbxPort
            // 
            this.TbxPort.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxPort.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxPort.Location = new System.Drawing.Point(124, 69);
            this.TbxPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxPort.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxPort.Name = "TbxPort";
            this.TbxPort.ShowText = false;
            this.TbxPort.Size = new System.Drawing.Size(185, 29);
            this.TbxPort.TabIndex = 13;
            this.TbxPort.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TbxPort.Watermark = "";
            // 
            // TbxStartAddress
            // 
            this.TbxStartAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxStartAddress.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxStartAddress.Location = new System.Drawing.Point(466, 69);
            this.TbxStartAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxStartAddress.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxStartAddress.Name = "TbxStartAddress";
            this.TbxStartAddress.ShowText = false;
            this.TbxStartAddress.Size = new System.Drawing.Size(185, 29);
            this.TbxStartAddress.TabIndex = 15;
            this.TbxStartAddress.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TbxStartAddress.Watermark = "";
            // 
            // TbxUnit
            // 
            this.TbxUnit.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxUnit.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxUnit.Location = new System.Drawing.Point(124, 113);
            this.TbxUnit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxUnit.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxUnit.Name = "TbxUnit";
            this.TbxUnit.ShowText = false;
            this.TbxUnit.Size = new System.Drawing.Size(185, 29);
            this.TbxUnit.TabIndex = 13;
            this.TbxUnit.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TbxUnit.Watermark = "";
            // 
            // TbxCount
            // 
            this.TbxCount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxCount.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxCount.Location = new System.Drawing.Point(466, 113);
            this.TbxCount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxCount.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxCount.Name = "TbxCount";
            this.TbxCount.ShowText = false;
            this.TbxCount.Size = new System.Drawing.Size(185, 29);
            this.TbxCount.TabIndex = 16;
            this.TbxCount.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TbxCount.Watermark = "";
            // 
            // TbxIpAddress
            // 
            this.TbxIpAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxIpAddress.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxIpAddress.Location = new System.Drawing.Point(124, 25);
            this.TbxIpAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxIpAddress.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxIpAddress.Name = "TbxIpAddress";
            this.TbxIpAddress.ShowText = false;
            this.TbxIpAddress.Size = new System.Drawing.Size(185, 29);
            this.TbxIpAddress.TabIndex = 12;
            this.TbxIpAddress.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.TbxIpAddress.Watermark = "";
            // 
            // BtnConnect
            // 
            this.BtnConnect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnConnect.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnConnect.Location = new System.Drawing.Point(32, 156);
            this.BtnConnect.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnConnect.Name = "BtnConnect";
            this.BtnConnect.Size = new System.Drawing.Size(119, 35);
            this.BtnConnect.TabIndex = 18;
            this.BtnConnect.Text = "Connect";
            this.BtnConnect.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnConnect.Click += new System.EventHandler(this.BtnConnect_Click);
            // 
            // BtnDisconnect
            // 
            this.BtnDisconnect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnDisconnect.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnDisconnect.Location = new System.Drawing.Point(180, 156);
            this.BtnDisconnect.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnDisconnect.Name = "BtnDisconnect";
            this.BtnDisconnect.Size = new System.Drawing.Size(119, 35);
            this.BtnDisconnect.TabIndex = 19;
            this.BtnDisconnect.Text = "Disconnect";
            this.BtnDisconnect.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnDisconnect.Click += new System.EventHandler(this.BtnDisconnect_Click);
            // 
            // BtnReadData
            // 
            this.BtnReadData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnReadData.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnReadData.Location = new System.Drawing.Point(328, 156);
            this.BtnReadData.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnReadData.Name = "BtnReadData";
            this.BtnReadData.Size = new System.Drawing.Size(154, 35);
            this.BtnReadData.TabIndex = 20;
            this.BtnReadData.Text = "StartReadData";
            this.BtnReadData.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnReadData.Click += new System.EventHandler(this.BtnReadData_Click);
            // 
            // BtnWriteData
            // 
            this.BtnWriteData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnWriteData.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnWriteData.Location = new System.Drawing.Point(511, 156);
            this.BtnWriteData.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnWriteData.Name = "BtnWriteData";
            this.BtnWriteData.Size = new System.Drawing.Size(119, 35);
            this.BtnWriteData.TabIndex = 21;
            this.BtnWriteData.Text = "WriteData";
            this.BtnWriteData.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnWriteData.Click += new System.EventHandler(this.BtnWriteData_Click);
            // 
            // timer
            // 
            this.timer.Interval = 2000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // TbxMessage
            // 
            this.TbxMessage.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbxMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TbxMessage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TbxMessage.Location = new System.Drawing.Point(0, 242);
            this.TbxMessage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TbxMessage.MinimumSize = new System.Drawing.Size(1, 16);
            this.TbxMessage.Multiline = true;
            this.TbxMessage.Name = "TbxMessage";
            this.TbxMessage.ShowScrollBar = true;
            this.TbxMessage.ShowText = false;
            this.TbxMessage.Size = new System.Drawing.Size(658, 236);
            this.TbxMessage.TabIndex = 22;
            this.TbxMessage.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.TbxMessage.Watermark = "";
            // 
            // uiPanel2
            // 
            this.uiPanel2.Controls.Add(this.label4);
            this.uiPanel2.Controls.Add(this.label5);
            this.uiPanel2.Controls.Add(this.label6);
            this.uiPanel2.Controls.Add(this.BtnDisconnect);
            this.uiPanel2.Controls.Add(this.BtnWriteData);
            this.uiPanel2.Controls.Add(this.TbxStartAddress);
            this.uiPanel2.Controls.Add(this.BtnReadData);
            this.uiPanel2.Controls.Add(this.TbxPort);
            this.uiPanel2.Controls.Add(this.label3);
            this.uiPanel2.Controls.Add(this.BtnConnect);
            this.uiPanel2.Controls.Add(this.TbxIpAddress);
            this.uiPanel2.Controls.Add(this.TbxUnit);
            this.uiPanel2.Controls.Add(this.label1);
            this.uiPanel2.Controls.Add(this.TbxCount);
            this.uiPanel2.Controls.Add(this.label2);
            this.uiPanel2.Controls.Add(this.TbxFunction);
            this.uiPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPanel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanel2.Location = new System.Drawing.Point(0, 34);
            this.uiPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(658, 208);
            this.uiPanel2.TabIndex = 3;
            this.uiPanel2.Text = null;
            this.uiPanel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(323, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "Count/Value:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(360, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 21);
            this.label5.TabIndex = 22;
            this.label5.Text = "Function:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(320, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 21);
            this.label6.TabIndex = 23;
            this.label6.Text = "StartAddress:";
            // 
            // CloseTimer
            // 
            this.CloseTimer.Interval = 25;
            this.CloseTimer.Tick += new System.EventHandler(this.CloseTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(658, 478);
            this.Controls.Add(this.uiPanel2);
            this.Controls.Add(this.TbxMessage);
            this.Controls.Add(this.uiPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1.PerformLayout();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UIPanel uiPanel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Sunny.UI.UITextBox TbxFunction;
        private Sunny.UI.UITextBox TbxPort;
        private Sunny.UI.UITextBox TbxStartAddress;
        private Sunny.UI.UITextBox TbxUnit;
        private Sunny.UI.UITextBox TbxCount;
        private Sunny.UI.UITextBox TbxIpAddress;
        private Sunny.UI.UIButton BtnConnect;
        private Sunny.UI.UIButton BtnDisconnect;
        private Sunny.UI.UIButton BtnReadData;
        private Sunny.UI.UIButton BtnWriteData;
        private System.Windows.Forms.Timer timer;
        private Sunny.UI.UITextBox TbxMessage;
        private Sunny.UI.UIButton BtnClose;
        private Sunny.UI.UIPanel uiPanel2;
        private System.Windows.Forms.Timer CloseTimer;
        private Sunny.UI.UIButton BtnHelp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Sunny.UI.UIButton BtnMin;
    }
}

