﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_ModbusTcpClient_1._0._1
{
    public partial class helpWindows : Form
    {
       private helpWindows()
        {
            InitializeComponent();
        }

        private void helpWindows_Load(object sender, EventArgs e)
        {

            TbxFunction.Text = "ModbusTcp使用功能说明：\r\n" +
                    "1.IpAddress：填写ModbusTcp服务端Ip地址。\r\n" +
                    "2.Port：填写ModbusTcp服务端的端口号。\r\n" +
                    "3.Unit：此参数为服务端单元标识符，默认填写1即可。\r\n" +
                    "4.Function：此参数为功能码，01读取输出线圈Bool  02读取输入Bool量 03读取保持寄存器 04读取输入寄存器 05写入单个线圈 06写入单个保存寄存器 15写入多个线圈 16写入多个保存寄存器。 \r\n \r\n" +
                    "5.StartAddress：此参数为起始地址。\r\n" +
                    "6.Count/Value：当功能码为01 02时，此处填写为要读取的线圈的个数 ； 当功能码为03 04时，此处为需要读取的寄存器个数 ； 当功能码为05 06时，此处为写入线圈或者寄存器的值；当功能码为15 16时，此处为需要写入的寄存器个数。\r\n \r\n" +
                    "7.Message：当功能码为读取功能码时，此处显示读取的数值。  注意！：读取线圈时,将会展示的个数为8*n  ； 当功能码为15 16时，此处为写入的数值，以英文','隔开，例如： 1,0,1,0 。\r\n \r\n" +
                    "感谢你的使用~";
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        Point downPoint_;
        void Panel_MouseDown(object sender, MouseEventArgs e)
        {
            //获取鼠标点击时的位置
            downPoint_ = new Point(e.X, e.Y);
        }

        void Panel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point RTPoint = MousePosition;
                RTPoint.Offset(-downPoint_.X, -downPoint_.Y);
                this.DesktopLocation=RTPoint;


            }
        }

        #region 单例模式
        static helpWindows singelHelpWindows = null;
        public static helpWindows GetHelpWindows()
        {
            if(singelHelpWindows == null)
            {
                singelHelpWindows = new helpWindows();
            }
            return singelHelpWindows;
        }
        #endregion



    }
}
