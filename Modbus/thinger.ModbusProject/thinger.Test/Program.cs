﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace thinger.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            // 如果是2个字节(A和B),要转换为2字节的整型的话,那么A*256+B=C
            byte[] bytes = { 0x1A, 0x0A };

            // ushort占2字节
            ushort result = (ushort)(bytes[0] * 256 + bytes[1]);

            Console.WriteLine($"1A0A转为整数的结果为:{result}");

            // 反过来,怎么取一个2字节的整数的高位和低位,最简的方式是高位除以256,低位取模256
            byte b1 = (byte)(result / 256);
            byte b2 = (byte)(result % 256);
            Console.WriteLine($"高位:{b1.ToString("X2")},低位:{b2.ToString("X2")}");

            Console.ReadKey();
        }
    }
}
