### Modbus基础知识概念

Modbus通信协议由Modicon公司（现在的施耐德电气Schneider Electric）于1979年为可编程逻辑控制（即PLC）通信而发表。目前，Modbus已经成为工业领域通信协议的业界标准，并且现在是工业电子设备之间常用的连接方式。Modbus作为目前工业领域应用最广泛的协议，与其他通信协议相比，有以下特点：  

- Modbus协议标准开放、公开发表且无版权要求。

- Modbus协议支持多种电气接口，包括RS232、RS485、TCP/IP等，还可以在各种介质上传输，如双绞线、光纤、红外、无线等。

- Modbus协议消息帧格式简单、紧凑、通俗易懂。

  用户理解和使用简单，厂商容易开发和集成，方便形成工业控制网络 Modbus协议是一种应用层报文传输协议，包括ASCII、RTU、TCP三种报文类型，协议本身并没有定义物理层，只是定义了控制器能够认识和使用的消息结构。

  Modbus协议使用串口传输时可以选择RTU或ASCII模式，并规定了消息、数据结构、命令和应答方式并需要对数据进行校验。

  ASCII 模式采用LRC校验，RTU模式采用16 位CRC校验，通过以太网传输时使用TCP，这种模式不使用校验，因为TCP协议是一个面向连接的可靠协议。

  Modbus协议规定了4个存储区，这里以常用的5段长度进行说明，如下表所示：

  ![image-20230821080831034](Modbus看这个就行了.assets/image-20230821080831034.png)

  Modbus协议同时规定了二十几种功能码，但是常用的只有8种，用于针对上述存储区的读写，如下表所示：

  ![image-20230821081058718](Modbus看这个就行了.assets/image-20230821081058718.png)

工欲善其事，必先利其器，Modbus学习配合相关的调试软件，可以达到事半功倍的效果。

Modbus学习必备的三大神器分别是Modbus Poll、Modbus Slave及VSPD，Modbus Poll软件主要用于仿真Modbus主站或Modbus客户端，Modbus Slave软件主要用于仿真Modbus从站或Modbus服务器，而VSPD全称Configure Virtual Serial Port Driver，是用来给电脑创建虚拟串口使用的。

### ModbusRTU/ASCII协议分析

ModbusRTU与ModbusASCII在报文数据发送格式上几乎一样，但也存在一些区别，具体体现在：

1、ModbusASCII有开始字符(:)和结束字符(CR LF)，可以作为一帧数据开始和结束的标志，而ModbusRTU没有这样的标志，需要用时间间隔来判断一帧报文的开始和结束，协议规定的时间为3.5个字符周期，就是说一帧报文开始前，必须有大于3.5个字符周期的空闲时间，一帧报文结束后，也必须要有3.5个字符周期的空闲时间否则就会出现粘包的情况。

注意：针对3.5个字符周期，其实是一个具体时间，但是这个时间跟波特率相关。在串口通信中，1个字符包括1位起始位、8位数据位（一般情况）、1位校验位（或者没有）、1位停止位（一般情况下），因此1个字符包括11个位，那么3.5个字符就是38.5个位，波特率表示的含义是每秒传输的二进制位的个位，因此如果是9600波特率，3.5个字符周期=1000/9600*38.5=4.01ms。

2、两者校验方式不同，ModbusRTU是CRC循环冗余校验，ModbusASCII是LCR纵向冗余校验。  

3、在Modbus标准中，RTU是必须要求的，而ASCII是可选项，即作为一个Modbus通信设备可以只支持RTU，也可以同时支持RTU和ASCII，但不能只支持ASCII。

下面针对具体报文进行分析，Modbus协议在串行链路上的报文格式如下所示：

![image-20230821081422014](Modbus看这个就行了.assets/image-20230821081422014.png)

### 读取输出线圈  

发送报文格式如下：  

![image-20230821081441665](Modbus看这个就行了.assets/image-20230821081441665.png)

发送报文含义：

读取1号从站输出线圈，起始地址为0x13=19，对应地址为00020，线圈数量为0x1B=27，即读取1号从站输出线圈，地址从00020-00046，共27个线圈的状态值。

注意：协议中的起始地址指的是索引，后面的地址指的是具体地址，对于任意一个存储区，索引都是从0开始的，但是对应的具体地址，与存储区是相关的，比如输出线圈，0对应00001；输入线圈，0对应10001；输入寄存器，0对应30001；保持寄存器，0对应40001。

返回报文格式如下：

![image-20230821081854553](Modbus看这个就行了.assets/image-20230821081854553.png)

返回1号从站输出线圈00020-00046，共27个线圈的状态值，返回字节数为4个，分别为CD 6B B2 05。

返回报文含义：

返回1号从站输出线圈00020-00046，共27个线圈的状态值，返回字节数为4个，分别为CD 6B B2 05。

CD=1100 1101 对应 00020-00027

6B=0110 1011 对应 00028-00035 

B2=1011 0010 对应 00036-00043

05=0000 0101 对应 00044-00046

### 读取输入线圈

发送报文格式如下：  

![image-20230821082524683](Modbus看这个就行了.assets/image-20230821082524683.png)

发送报文含义：  

读取1号从站输入线圈，起始地址为0xC4=196，对应地址为10197，线圈数量为0x1D=29，即读取1号从站输入线圈，地址从10197-10225，共29个线圈的状态值。返回报文格式如下：

![image-20230821082628254](Modbus看这个就行了.assets/image-20230821082628254.png)

返回报文含义：

返回1号从站输入线圈10197-10225，共29个线圈的状态值，返回字节数为4个，分别为CD 6B B2 05。

CD=1100 1101 对应 10197-10204

6B=0110 1011 对应 10205-10212 

B2=1011 0010 对应 10213-10220

05=0000 0101 对应 10221-10225

### 读取保持寄存器

发送报文格式如下：

![image-20230821082820147](Modbus看这个就行了.assets/image-20230821082820147.png)

发送报文含义：

读取1号从站保持寄存器，起始地址为0x6B=107，对应地址为40108，寄存器数量为0x02=2，即读取1号从站保持寄存器，地址从40108-40109，共2个寄存器的数值。返回报文格式如下：

![image-20230821082833146](Modbus看这个就行了.assets/image-20230821082833146.png)

返回报文含义：

返回1号从站保持寄存器40108-40109，共2个寄存器的数值，返回字节数为4个，分别为02 2B 01 06。

40108对应数值为0x022B，

40109对应数值为0x0106。

### 读取输入寄存器

发送报文格式如下：

![image-20230821083028830](Modbus看这个就行了.assets/image-20230821083028830.png)

发送报文含义：

读取1号从站输入寄存器，起始地址为0x6B=107，对应地址为30108，寄存器数量为0x02=2，即读取1号从站输入寄存器，地址从30108-30109，共2个寄存器的数值。返回报文格式如下：

![image-20230821083039546](Modbus看这个就行了.assets/image-20230821083039546.png)

返回报文含义：

返回1号从站输入寄存器30108-30109，共2个寄存器的数值，返回字节数为4个，分别为02 2B 01 06。

30108对应数值为0x022B，

30109对应数值为0x0106。

### 预置单线圈  

发送报文格式如下：

![image-20230821083117082](Modbus看这个就行了.assets/image-20230821083117082.png)

发送报文含义：

预置1号从站单个线圈的值，线圈地址为0x00AC=172，对应地址为00173，断通标志0xFF00表示置位，0x0000表示复位，即置位1号从站输出线圈00173。返回报文格式如下：

![image-20230821083130544](Modbus看这个就行了.assets/image-20230821083130544.png)

返回报文含义：

预置单输出线圈原报文返回。

### 预置单寄存器

发送报文格式如下：

![image-20230821083155895](Modbus看这个就行了.assets/image-20230821083155895.png)

发送报文含义：

预置1号从站单个保持寄存器的值，寄存器地址为0x0087=135，对应地址为40136，写入值为0x039E，即预置1号从站保持寄存器40136值为0x039E。

返回报文格式如下：

![image-20230821083211265](Modbus看这个就行了.assets/image-20230821083211265.png)

返回报文含义：

预置单保持寄存器原报文返回。

### 预置多线圈

发送报文格式如下：请忽略这张图

![image-20230821083230163](Modbus看这个就行了.assets/image-20230821083230163.png)

发送报文含义：

预置1号从站多个线圈的值，线圈地址为0x0013=19，对应地址为00020，线圈数为0x0A=10，写入值为0xCD00，即预置1号从站线圈00020-00027=0xCD=1100 1101，00028-00029=0x00=0000 0000。

返回报文格式如下：

![image-20230821083244947](Modbus看这个就行了.assets/image-20230821083244947.png)

返回报文含义：

预置多输出线圈返回报文是在原报文基础上除去字节数及具体字节后返回。

### 预置多寄存器

发送报文格式如下：

![image-20230821083302106](Modbus看这个就行了.assets/image-20230821083302106.png)

发送报文含义：

预置1号从站多个寄存器的值，寄存器地址为0x0087=135，起始地址为40136，寄存器数量为0x02=2，结束地址为40137，写入值为0x0105和0x0A10，即预置1号从站寄存器40136=0x0105，40137=0x0A10。

返回报文格式如下：

![image-20230821083318403](Modbus看这个就行了.assets/image-20230821083318403.png)

返回报文含义：

预置多保持寄存器返回报文是在原报文基础上除去字节数及具体字节后返回。