﻿
namespace Slave
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.SerialConfig_gbx = new System.Windows.Forms.GroupBox();
            this.stop_lab = new System.Windows.Forms.Label();
            this.parity_lab = new System.Windows.Forms.Label();
            this.data_lab = new System.Windows.Forms.Label();
            this.baud_lab = new System.Windows.Forms.Label();
            this.port_lab = new System.Windows.Forms.Label();
            this.serialStopbit_cbb = new System.Windows.Forms.ComboBox();
            this.connect_btn = new System.Windows.Forms.Button();
            this.serialParity_cbb = new System.Windows.Forms.ComboBox();
            this.serialDatabit_cbb = new System.Windows.Forms.ComboBox();
            this.serialBaud_cbb = new System.Windows.Forms.ComboBox();
            this.serialPort_cbb = new System.Windows.Forms.ComboBox();
            this.Coli_gbx = new System.Windows.Forms.GroupBox();
            this.Input_gbx = new System.Windows.Forms.GroupBox();
            this.HoldingRegister_gbx = new System.Windows.Forms.GroupBox();
            this.InputRegister_gbx = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.message_tssl = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.SerialConfig_gbx.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // SerialConfig_gbx
            // 
            this.SerialConfig_gbx.Controls.Add(this.stop_lab);
            this.SerialConfig_gbx.Controls.Add(this.parity_lab);
            this.SerialConfig_gbx.Controls.Add(this.data_lab);
            this.SerialConfig_gbx.Controls.Add(this.baud_lab);
            this.SerialConfig_gbx.Controls.Add(this.port_lab);
            this.SerialConfig_gbx.Controls.Add(this.serialStopbit_cbb);
            this.SerialConfig_gbx.Controls.Add(this.connect_btn);
            this.SerialConfig_gbx.Controls.Add(this.serialParity_cbb);
            this.SerialConfig_gbx.Controls.Add(this.serialDatabit_cbb);
            this.SerialConfig_gbx.Controls.Add(this.serialBaud_cbb);
            this.SerialConfig_gbx.Controls.Add(this.serialPort_cbb);
            this.SerialConfig_gbx.Location = new System.Drawing.Point(12, 12);
            this.SerialConfig_gbx.Name = "SerialConfig_gbx";
            this.SerialConfig_gbx.Size = new System.Drawing.Size(413, 98);
            this.SerialConfig_gbx.TabIndex = 6;
            this.SerialConfig_gbx.TabStop = false;
            this.SerialConfig_gbx.Text = "串口配置";
            // 
            // stop_lab
            // 
            this.stop_lab.AutoSize = true;
            this.stop_lab.Location = new System.Drawing.Point(173, 74);
            this.stop_lab.Name = "stop_lab";
            this.stop_lab.Size = new System.Drawing.Size(41, 12);
            this.stop_lab.TabIndex = 15;
            this.stop_lab.Text = "停止位";
            // 
            // parity_lab
            // 
            this.parity_lab.AutoSize = true;
            this.parity_lab.Location = new System.Drawing.Point(173, 48);
            this.parity_lab.Name = "parity_lab";
            this.parity_lab.Size = new System.Drawing.Size(41, 12);
            this.parity_lab.TabIndex = 14;
            this.parity_lab.Text = "校验位";
            // 
            // data_lab
            // 
            this.data_lab.AutoSize = true;
            this.data_lab.Location = new System.Drawing.Point(6, 73);
            this.data_lab.Name = "data_lab";
            this.data_lab.Size = new System.Drawing.Size(41, 12);
            this.data_lab.TabIndex = 13;
            this.data_lab.Text = "数据位";
            // 
            // baud_lab
            // 
            this.baud_lab.AutoSize = true;
            this.baud_lab.Location = new System.Drawing.Point(6, 48);
            this.baud_lab.Name = "baud_lab";
            this.baud_lab.Size = new System.Drawing.Size(41, 12);
            this.baud_lab.TabIndex = 12;
            this.baud_lab.Text = "波特率";
            // 
            // port_lab
            // 
            this.port_lab.AutoSize = true;
            this.port_lab.Location = new System.Drawing.Point(6, 23);
            this.port_lab.Name = "port_lab";
            this.port_lab.Size = new System.Drawing.Size(41, 12);
            this.port_lab.TabIndex = 11;
            this.port_lab.Text = "端口号";
            // 
            // serialStopbit_cbb
            // 
            this.serialStopbit_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialStopbit_cbb.FormattingEnabled = true;
            this.serialStopbit_cbb.Location = new System.Drawing.Point(226, 70);
            this.serialStopbit_cbb.Name = "serialStopbit_cbb";
            this.serialStopbit_cbb.Size = new System.Drawing.Size(91, 20);
            this.serialStopbit_cbb.TabIndex = 5;
            // 
            // connect_btn
            // 
            this.connect_btn.Location = new System.Drawing.Point(323, 18);
            this.connect_btn.Name = "connect_btn";
            this.connect_btn.Size = new System.Drawing.Size(75, 46);
            this.connect_btn.TabIndex = 6;
            this.connect_btn.Text = "启动";
            this.connect_btn.UseVisualStyleBackColor = true;
            this.connect_btn.Click += new System.EventHandler(this.connect_btn_Click);
            // 
            // serialParity_cbb
            // 
            this.serialParity_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialParity_cbb.FormattingEnabled = true;
            this.serialParity_cbb.Location = new System.Drawing.Point(226, 45);
            this.serialParity_cbb.Name = "serialParity_cbb";
            this.serialParity_cbb.Size = new System.Drawing.Size(91, 20);
            this.serialParity_cbb.TabIndex = 4;
            // 
            // serialDatabit_cbb
            // 
            this.serialDatabit_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialDatabit_cbb.FormattingEnabled = true;
            this.serialDatabit_cbb.Location = new System.Drawing.Point(58, 70);
            this.serialDatabit_cbb.Name = "serialDatabit_cbb";
            this.serialDatabit_cbb.Size = new System.Drawing.Size(91, 20);
            this.serialDatabit_cbb.TabIndex = 3;
            // 
            // serialBaud_cbb
            // 
            this.serialBaud_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialBaud_cbb.FormattingEnabled = true;
            this.serialBaud_cbb.Location = new System.Drawing.Point(58, 44);
            this.serialBaud_cbb.Name = "serialBaud_cbb";
            this.serialBaud_cbb.Size = new System.Drawing.Size(91, 20);
            this.serialBaud_cbb.TabIndex = 2;
            // 
            // serialPort_cbb
            // 
            this.serialPort_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialPort_cbb.FormattingEnabled = true;
            this.serialPort_cbb.Location = new System.Drawing.Point(58, 20);
            this.serialPort_cbb.Name = "serialPort_cbb";
            this.serialPort_cbb.Size = new System.Drawing.Size(259, 20);
            this.serialPort_cbb.TabIndex = 1;
            // 
            // Coli_gbx
            // 
            this.Coli_gbx.Location = new System.Drawing.Point(13, 117);
            this.Coli_gbx.Name = "Coli_gbx";
            this.Coli_gbx.Size = new System.Drawing.Size(735, 137);
            this.Coli_gbx.TabIndex = 7;
            this.Coli_gbx.TabStop = false;
            this.Coli_gbx.Text = "线圈状态(Coil state )";
            // 
            // Input_gbx
            // 
            this.Input_gbx.Location = new System.Drawing.Point(12, 260);
            this.Input_gbx.Name = "Input_gbx";
            this.Input_gbx.Size = new System.Drawing.Size(736, 137);
            this.Input_gbx.TabIndex = 8;
            this.Input_gbx.TabStop = false;
            this.Input_gbx.Text = "离散输入状态(Discrete input state)";
            // 
            // HoldingRegister_gbx
            // 
            this.HoldingRegister_gbx.Location = new System.Drawing.Point(12, 403);
            this.HoldingRegister_gbx.Name = "HoldingRegister_gbx";
            this.HoldingRegister_gbx.Size = new System.Drawing.Size(736, 101);
            this.HoldingRegister_gbx.TabIndex = 9;
            this.HoldingRegister_gbx.TabStop = false;
            this.HoldingRegister_gbx.Text = "保持寄存器状态(Holding register state)";
            // 
            // InputRegister_gbx
            // 
            this.InputRegister_gbx.Location = new System.Drawing.Point(13, 510);
            this.InputRegister_gbx.Name = "InputRegister_gbx";
            this.InputRegister_gbx.Size = new System.Drawing.Size(736, 101);
            this.InputRegister_gbx.TabIndex = 10;
            this.InputRegister_gbx.TabStop = false;
            this.InputRegister_gbx.Text = "输入寄存器状态(Input register state)";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.message_tssl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 617);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(760, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(44, 17);
            this.toolStripStatusLabel1.Text = "状态：";
            // 
            // message_tssl
            // 
            this.message_tssl.AutoSize = false;
            this.message_tssl.Name = "message_tssl";
            this.message_tssl.Size = new System.Drawing.Size(300, 17);
            this.message_tssl.Text = "软件启动成功！";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.linkLabel2);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Location = new System.Drawing.Point(431, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 99);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(113, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(198, 85);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(6, 53);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(53, 12);
            this.linkLabel2.TabIndex = 2;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "必应链接";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(6, 28);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(53, 12);
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "百度链接";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 639);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.InputRegister_gbx);
            this.Controls.Add(this.HoldingRegister_gbx);
            this.Controls.Add(this.Input_gbx);
            this.Controls.Add(this.Coli_gbx);
            this.Controls.Add(this.SerialConfig_gbx);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Main";
            this.Text = "NmodbusSlave";
            this.Load += new System.EventHandler(this.Main_Load);
            this.SerialConfig_gbx.ResumeLayout(false);
            this.SerialConfig_gbx.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox SerialConfig_gbx;
        private System.Windows.Forms.Label stop_lab;
        private System.Windows.Forms.Label parity_lab;
        private System.Windows.Forms.Label data_lab;
        private System.Windows.Forms.Label baud_lab;
        private System.Windows.Forms.Label port_lab;
        private System.Windows.Forms.ComboBox serialStopbit_cbb;
        private System.Windows.Forms.Button connect_btn;
        private System.Windows.Forms.ComboBox serialParity_cbb;
        private System.Windows.Forms.ComboBox serialDatabit_cbb;
        private System.Windows.Forms.ComboBox serialBaud_cbb;
        private System.Windows.Forms.ComboBox serialPort_cbb;
        private System.Windows.Forms.GroupBox Coli_gbx;
        private System.Windows.Forms.GroupBox Input_gbx;
        private System.Windows.Forms.GroupBox HoldingRegister_gbx;
        private System.Windows.Forms.GroupBox InputRegister_gbx;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel message_tssl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

