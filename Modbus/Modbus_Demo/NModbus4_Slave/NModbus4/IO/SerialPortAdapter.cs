namespace Modbus.IO
{
    using System;
    using System.Diagnostics;
    using System.IO.Ports;

    using Unme.Common;

    /// <summary>
    /// 串口适配器，实现<see cref="IStreamResource"/>接口，
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    internal class SerialPortAdapter : IStreamResource
    {
        /// <summary>
        /// 串口通信对象。
        /// </summary>
        private SerialPort _serialPort;

        /// <summary>
        /// 初始化串口对象适配器实例。
        /// </summary>
        /// <param name="serialPort">串口对象</param>
        public SerialPortAdapter(SerialPort serialPort)
        {
            Debug.Assert(serialPort != null, "Argument serialPort cannot be null.");

            _serialPort = serialPort;
            _serialPort.NewLine = Modbus.NewLine;
        }
        /// <summary>
        /// 无限延时。
        /// </summary>
        public int InfiniteTimeout
        {
            get { return SerialPort.InfiniteTimeout; }
        }
        /// <summary>
        /// 读数据超时。
        /// </summary>
        public int ReadTimeout
        {
            get { return _serialPort.ReadTimeout; }
            set { _serialPort.ReadTimeout = value; }
        }
        /// <summary>
        /// 写数据超时。
        /// </summary>
        public int WriteTimeout
        {
            get { return _serialPort.WriteTimeout; }
            set { _serialPort.WriteTimeout = value; }
        }
        /// <summary>
        /// 缓冲区中丢弃
        /// </summary>
        public void DiscardInBuffer()
        {
            _serialPort.DiscardInBuffer();
        }

        /// <summary>
        /// 读数据。
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">偏移量</param>
        /// <param name="count">数量</param>
        /// <returns>读取数量</returns>
        public int Read(byte[] buffer, int offset, int count)
        {
            return _serialPort.Read(buffer, offset, count);
        }

        /// <summary>
        /// 写数据。
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">偏移量</param>
        /// <param name="count">数量</param>
        public void Write(byte[] buffer, int offset, int count)
        {
            _serialPort.Write(buffer, offset, count);
        }
        /// <summary>
        /// 资源释放。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// 资源释放。
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposableUtility.Dispose(ref _serialPort);
        }
    }
}
