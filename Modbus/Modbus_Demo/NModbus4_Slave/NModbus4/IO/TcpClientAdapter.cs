namespace Modbus.IO
{
    using System;
    using System.Diagnostics;
    using System.Net.Sockets;
    using System.Threading;

    using Unme.Common;

    /// <summary>
    /// TCP客户端适配器，实现<see cref="IStreamResource"/>接口，
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    internal class TcpClientAdapter : IStreamResource
    {
        /// <summary>
        /// tcp客户端。
        /// </summary>
        private TcpClient _tcpClient;

        /// <summary>
        /// 初始化TCP客户端适配器实例。
        /// </summary>
        /// <param name="tcpClient">tcp客户端</param>
        public TcpClientAdapter(TcpClient tcpClient)
        {
            Debug.Assert(tcpClient != null, "Argument tcpClient cannot be null.");

            _tcpClient = tcpClient;
        }

        /// <summary>
        /// 无限延时。
        /// </summary>
        public int InfiniteTimeout
        {
            get { return Timeout.Infinite; }
        }
        /// <summary>
        /// 读数据超时。
        /// </summary>
        public int ReadTimeout
        {
            get { return _tcpClient.GetStream().ReadTimeout; }
            set { _tcpClient.GetStream().ReadTimeout = value; }
        }
        /// <summary>
        /// 写数据超时。
        /// </summary>
        public int WriteTimeout
        {
            get { return _tcpClient.GetStream().WriteTimeout; }
            set { _tcpClient.GetStream().WriteTimeout = value; }
        }
        /// <summary>
        /// 写数据。
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">偏移量</param>
        /// <param name="size">数量</param>
        public void Write(byte[] buffer, int offset, int size)
        {
            _tcpClient.GetStream().Write(buffer, offset, size);
        }
        /// <summary>
        /// 读数据。
        /// </summary>
        /// <param name="buffer">字节数组</param>
        /// <param name="offset">偏移量</param>
        /// <param name="size">数量</param>
        /// <returns>读取数量</returns>
        public int Read(byte[] buffer, int offset, int size)
        {
            return _tcpClient.GetStream().Read(buffer, offset, size);
        }
        /// <summary>
        /// 缓冲区中丢弃
        /// </summary>
        public void DiscardInBuffer()
        {
            _tcpClient.GetStream().Flush();
        }
        /// <summary>
        /// 资源释放。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// 资源释放。
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposableUtility.Dispose(ref _tcpClient);
        }
    }
}
