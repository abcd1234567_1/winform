﻿namespace Modbus.IO
{
    using System;

    using Message;
    /// <summary>
    /// 空的占位类。
    /// </summary>
    public class EmptyTransport : ModbusTransport
    {
        /// <summary>
        /// 读请求消息数组。
        /// </summary>
        /// <returns>返回数组</returns>
        internal override byte[] ReadRequest()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 读取响应消息。
        /// </summary>
        /// <typeparam name="T">modbus消息类型</typeparam>
        /// <returns>返回响应消息</returns>
        internal override IModbusMessage ReadResponse<T>()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 创建消息帧。
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        internal override byte[] BuildMessageFrame(Message.IModbusMessage message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 写消息帧。
        /// </summary>
        /// <param name="message">需要写入的消息</param>
        internal override void Write(IModbusMessage message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 验证消息包。
        /// </summary>
        /// <param name="request">请求消息包</param>
        /// <param name="response">响应消息包</param>
        internal override void OnValidateResponse(IModbusMessage request, IModbusMessage response)
        {
            throw new NotImplementedException();
        }
    }
}
