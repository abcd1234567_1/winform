﻿namespace Modbus.Unme.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    /// <summary>
    /// 序列实用程序
    /// </summary>
    internal static class SequenceUtility
    {
        /// <summary>
        /// 截取数据
        /// </summary>
        /// <typeparam name="T">数据类型</typeparam>
        /// <param name="source">原始数据</param>
        /// <param name="startIndex">开始索引</param>
        /// <param name="size">数量</param>
        /// <returns>返回输出T类型数据</returns>
        public static IEnumerable<T> Slice<T>(this IEnumerable<T> source, int startIndex, int size)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            var enumerable = source as T[] ?? source.ToArray();
            int num = enumerable.Count();
            if (startIndex < 0 || num < startIndex)
                throw new ArgumentOutOfRangeException("startIndex");
            if (size < 0 || startIndex + size > num)
                throw new ArgumentOutOfRangeException("size");

            return enumerable.Skip(startIndex).Take(size);
        }
    }
}
