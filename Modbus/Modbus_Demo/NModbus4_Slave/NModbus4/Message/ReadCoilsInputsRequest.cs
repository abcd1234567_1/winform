namespace Modbus.Message
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Net;

    /// <summary>
    /// 读取线程、离散输入请求类。
    /// </summary>
    public class ReadCoilsInputsRequest : AbstractModbusMessage, IModbusRequest
    {
        /// <summary>
        /// 初始化读取线程、离散输入请求实例。
        /// </summary>
        public ReadCoilsInputsRequest()
        {
        }

        /// <summary>
        /// 使用(功能码，从设备地址，起始地址，数据数量)，初始化读取线程、离散输入请求实例。
        /// </summary>
        /// <param name="functionCode">功能码</param>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startAddress">起始地址</param>
        /// <param name="numberOfPoints">数据数量</param>
        public ReadCoilsInputsRequest(byte functionCode, byte slaveAddress, ushort startAddress, ushort numberOfPoints)
            : base(slaveAddress, functionCode)
        {
            StartAddress = startAddress;
            NumberOfPoints = numberOfPoints;
        }

        /// <summary>
        /// 起始地址。
        /// </summary>
        public ushort StartAddress
        {
            get { return MessageImpl.StartAddress.Value; }
            set { MessageImpl.StartAddress = value; }
        }

        /// <summary>
        /// 帧最小长度。
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 6; }
        }

        /// <summary>
        /// 数据数量。
        /// </summary>
        public ushort NumberOfPoints
        {
            get { return MessageImpl.NumberOfPoints.Value; }
            set
            {
                if (value > Modbus.MaximumDiscreteRequestResponseSize)
                    throw new ArgumentOutOfRangeException("NumberOfPoints",
                        String.Format(CultureInfo.InvariantCulture, "Maximum amount of data {0} coils.",
                            Modbus.MaximumDiscreteRequestResponseSize));

                MessageImpl.NumberOfPoints = value;
            }
        }

        /// <summary>
        /// 消息转字符串。
        /// </summary>
        /// <returns>返回消息字符串</returns>
        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "Read {0} {1} starting at address {2}.", NumberOfPoints,
                FunctionCode == Modbus.ReadCoils ? "coils" : "inputs", StartAddress);
        }

        /// <summary>
        /// 验证响应消息。
        /// </summary>
        /// <param name="response">modbus消息，参考<see cref="IModbusMessage"/></param>
        public void ValidateResponse(IModbusMessage response)
        {
            var typedResponse = (ReadCoilsInputsResponse) response;

            // best effort validation - the same response for a request for 1 vs 6 coils (same byte count) will pass validation.
            var expectedByteCount = (NumberOfPoints + 7)/8;
            if (expectedByteCount != typedResponse.ByteCount)
            {
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected byte count. Expected {0}, received {1}.",
                    expectedByteCount,
                    typedResponse.ByteCount));
            }
        }

        /// <summary>
        /// 特殊初始化。
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            StartAddress = (ushort) IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));
            NumberOfPoints = (ushort) IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 4));
        }
    }
}
