namespace Modbus.Device
{
    using System;

    using IO;

    using Unme.Common;

    /// <summary>
    /// Modbus设备类，
    /// </summary>
    public abstract class ModbusDevice : IDisposable
    {
        /// <summary>
        ///  设备中的传输对象。
        /// </summary>
        private ModbusTransport _transport;

        /// <summary>
        /// 初始化传输实例
        /// </summary>
        /// <param name="transport">传输对象</param>
        internal ModbusDevice(ModbusTransport transport)
        {
            _transport = transport;
        }

        /// <summary>
        ///  获取Modbus传输对象。
        /// </summary>
        /// <value>传输对象</value>
        public ModbusTransport Transport
        {
            get { return _transport; }
        }

        /// <summary>
        /// 释放非托管资源和(可选)托管资源
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  释放非托管资源和(可选)托管资源
        /// </summary>
        /// <param name="disposing">
        ///  true释放托管和非托管资源;false只释放非托管资源。
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposableUtility.Dispose(ref _transport);
        }
    }
}
