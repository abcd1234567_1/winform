﻿namespace Modbus.Device
{
    using System;

    using Message;

    /// <summary>
    ///  Modbus 从设备请求事件参数包含消息的信息。
    /// </summary>
    public class ModbusSlaveRequestEventArgs : EventArgs
    {
        /// <summary>
        /// modbus消息实例。
        /// </summary>
        private readonly IModbusMessage _message;

        /// <summary>
        /// 初始化modbus从设备请求事件参数实例。
        /// </summary>
        /// <param name="message"></param>
        internal ModbusSlaveRequestEventArgs(IModbusMessage message)
        {
            _message = message;
        }

        /// <summary>
        ///  获取modbus消息。
        /// </summary>
        /// <value>对应消息</value>
        public IModbusMessage Message
        {
            get { return _message; }
        }
    }
}
