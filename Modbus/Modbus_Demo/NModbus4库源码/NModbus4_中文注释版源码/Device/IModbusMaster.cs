namespace Modbus.Device
{
    using System;
    using System.Threading.Tasks;

    using IO;

    /// <summary>
    ///  modbus主设备接口。
    /// </summary>
    public interface IModbusMaster : IDisposable
    {
        /// <summary>
        ///  主设备中的传输对象。
        /// </summary>
        ModbusTransport Transport { get; }

        /// <summary>
        /// 读取从1到2000的线圈状态
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>线圈数据</returns>
        bool[] ReadCoils(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        /// 异步读取从1到2000的线圈状态   
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        Task<bool[]> ReadCoilsAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        ///    读取1到2000离散输入的状态
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>离散数据</returns>
        bool[] ReadInputs(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        ///    异步读取1到2000离散输入的状态
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        Task<bool[]> ReadInputsAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        ///    读取保持寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>保持寄存器数据</returns>
        ushort[] ReadHoldingRegisters(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        ///    异步读取保持寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        Task<ushort[]> ReadHoldingRegistersAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        /// 读取输入寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>输入寄存器数据</returns>
        ushort[] ReadInputRegisters(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        /// 异步读取输入寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        Task<ushort[]> ReadInputRegistersAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints);

        /// <summary>
        ///  写单个线圈。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="coilAddress">要写入起始地址</param>
        /// <param name="value">要写入的值</param>
        void WriteSingleCoil(byte slaveAddress, ushort coilAddress, bool value);

        /// <summary>
        ///  异步写单个线圈。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="coilAddress">要写入起始地址</param>
        /// <param name="value">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        Task WriteSingleCoilAsync(byte slaveAddress, ushort coilAddress, bool value);

        /// <summary>
        ///  写单个保持寄存器。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="registerAddress">要写入的起始地址</param>
        /// <param name="value">要写入的值</param>
        void WriteSingleRegister(byte slaveAddress, ushort registerAddress, ushort value);

        /// <summary>
        ///  异步写入单个保持寄存器。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="registerAddress">要写入的起始地址</param>
        /// <param name="value">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        Task WriteSingleRegisterAsync(byte slaveAddress, ushort registerAddress, ushort value);

        /// <summary>
        ///  写多个保持寄存器数据（1到123，限制了数据，如要扩展需要修改）
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        void WriteMultipleRegisters(byte slaveAddress, ushort startAddress, ushort[] data);

        /// <summary>
        ///  异步写多个保持寄存器数据（1到123，限制了数据，如要扩展需要修改）
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        Task WriteMultipleRegistersAsync(byte slaveAddress, ushort startAddress, ushort[] data);

        /// <summary>
        ///  写多个线圈
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        void WriteMultipleCoils(byte slaveAddress, ushort startAddress, bool[] data);

        /// <summary>
        ///  异步写多个线圈
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        Task WriteMultipleCoilsAsync(byte slaveAddress, ushort startAddress, bool[] data);

        /// <summary>
        ///   在单个Modbus事务中执行一个读操作和一个写操作的组合。写操作在读操作之前执行。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startReadAddress">开始读取的地址(保存寄存器从0开始寻址)</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数目</param>
        /// <param name="startWriteAddress">开始写入的地址(保存寄存器从0开始寻址)</param>
        /// <param name="writeData">需要写入的值</param>
        ushort[] ReadWriteMultipleRegisters(byte slaveAddress, ushort startReadAddress, ushort numberOfPointsToRead,
            ushort startWriteAddress, ushort[] writeData);

        /// <summary>
        ///   在单个Modbus事务中执行一个读操作和一个写操作的组合。写操作在读操作之前执行。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startReadAddress">开始读取的地址(保存寄存器从0开始寻址)</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数目</param>
        /// <param name="startWriteAddress">开始写入的地址(保存寄存器从0开始寻址)</param>
        /// <param name="writeData">需要写入的值</param>
        /// <returns>返回异步操作的任务</returns>
        Task<ushort[]> ReadWriteMultipleRegistersAsync(byte slaveAddress, ushort startReadAddress, ushort numberOfPointsToRead,
            ushort startWriteAddress, ushort[] writeData);
    }
}
