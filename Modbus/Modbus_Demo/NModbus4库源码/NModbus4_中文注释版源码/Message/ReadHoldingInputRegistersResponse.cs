namespace Modbus.Message
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Data;
    using Unme.Common;

    /// <summary>
    /// 读取保持和输入寄存器响应消息类。
    /// </summary>
    public class ReadHoldingInputRegistersResponse : AbstractModbusMessageWithData<RegisterCollection>, IModbusMessage
    {
        /// <summary>
        /// 初始化读取保持、输入寄存器响应实例。
        /// </summary>
        public ReadHoldingInputRegistersResponse()
        {
        }

        /// <summary>
        /// 使用(功能码，从设备地址，数据集合)，初始化读取保持、输入寄存器响应实例。
        /// </summary>
        /// <param name="functionCode">功能码</param>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="data">数据集合<see cref="RegisterCollection"/></param>
        public ReadHoldingInputRegistersResponse(byte functionCode, byte slaveAddress, RegisterCollection data)
            : base(slaveAddress, functionCode)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            ByteCount = data.ByteCount;
            Data = data;
        }

        /// <summary>
        /// 字节数量。
        /// </summary>
        public byte ByteCount
        {
            get { return MessageImpl.ByteCount.Value; }
            set { MessageImpl.ByteCount = value; }
        }

        /// <summary>
        /// 帧最小长度。
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 3; }
        }

        /// <summary>
        /// 消息转字符串。
        /// </summary>
        /// <returns>消息字符串</returns>
        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "Read {0} {1} registers.", Data.Count,
                FunctionCode == Modbus.ReadHoldingRegisters ? "holding" : "input");
        }

        /// <summary>
        /// 特殊初始化。
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            if (frame.Length < MinimumFrameSize + frame[2])
                throw new FormatException("Message frame does not contain enough bytes.");

            ByteCount = frame[2];
            Data = new RegisterCollection(frame.Slice(3, ByteCount).ToArray());
        }
    }
}
