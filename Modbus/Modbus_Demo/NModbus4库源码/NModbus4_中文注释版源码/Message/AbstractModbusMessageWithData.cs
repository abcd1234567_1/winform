namespace Modbus.Message
{
    using Data;

    /// <summary>
    /// 带TData数据的Modbus消息抽象类。
    /// </summary>
    /// <typeparam name="TData">数据<see cref="IModbusMessageDataCollection"/></typeparam>
    public abstract class AbstractModbusMessageWithData<TData> : AbstractModbusMessage where TData : IModbusMessageDataCollection
    {
        /// <summary>
        /// 带数据的modbus消息实例抽象方法。
        /// </summary>
        internal AbstractModbusMessageWithData()
        {
        }

        /// <summary>
        /// 使用(从设备地址，功能码)的带数据的modbus消息实例抽象方法。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="functionCode">功能码</param>
        internal AbstractModbusMessageWithData(byte slaveAddress, byte functionCode)
            : base(slaveAddress, functionCode)
        {
        }

        /// <summary>
        /// 消息帧的数据集
        /// </summary>
        public TData Data
        {
            get { return (TData) MessageImpl.Data; }
            set { MessageImpl.Data = value; }
        }
    }
}
