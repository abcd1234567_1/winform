namespace Modbus.Message
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// modbus消息接口，主设备(客户端)构建的发起Modbus事务的消息。
    /// </summary>
    public interface IModbusMessage
    {
        /// <summary>
        ///  功能码，告诉服务器要执行的操作。
        /// </summary>
        byte FunctionCode { get; set; }

        /// <summary>
        ///  从设备(服务端)地址。
        /// </summary>
        byte SlaveAddress { get; set; }

        /// <summary>
        ///  由从设备地址和协议数据单元组（PDU）成的消息帧。
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        byte[] MessageFrame { get; }

        /// <summary>
        ///  功能代码和消息数据的组成的协议数据单元（PDU）。
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        byte[] ProtocolDataUnit { get; }

        /// <summary>
        /// 使用IP协议时分配给消息的唯一标识符。
        /// </summary>
        ushort TransactionId { get; set; }

        /// <summary>
        /// 从指定的消息帧初始化modbus消息。
        /// </summary>
        /// <param name="frame">消息帧</param>
        void Initialize(byte[] frame);
    }
}
