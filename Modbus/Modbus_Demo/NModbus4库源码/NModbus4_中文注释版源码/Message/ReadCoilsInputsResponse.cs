namespace Modbus.Message
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Data;

    using Unme.Common;

    /// <summary>
    /// 读线圈、离散输入响应类。
    /// </summary>
    public class ReadCoilsInputsResponse : AbstractModbusMessageWithData<DiscreteCollection>, IModbusMessage
    {
        /// <summary>
        /// 初始化读线圈、离散输入响应实例。
        /// </summary>
        public ReadCoilsInputsResponse()
        {
        }

        /// <summary>
        /// 使用(功能码，从设备地址，字节数量，数据集合)，初始化读线圈、离散输入响应实例。
        /// </summary>
        /// <param name="functionCode"></param>
        /// <param name="slaveAddress"></param>
        /// <param name="byteCount"></param>
        /// <param name="data"></param>
        public ReadCoilsInputsResponse(byte functionCode, byte slaveAddress, byte byteCount, DiscreteCollection data)
            : base(slaveAddress, functionCode)
        {
            ByteCount = byteCount;
            Data = data;
        }

        /// <summary>
        /// 字节数量。
        /// </summary>
        public byte ByteCount
        {
            get { return MessageImpl.ByteCount.Value; }
            set { MessageImpl.ByteCount = value; }
        }

        /// <summary>
        /// 帧最小长度。
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 3; }
        }

        /// <summary>
        /// 消息转字符串。
        /// </summary>
        /// <returns>返回消息字符串</returns>
        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "Read {0} {1} - {2}.",
                Data.Count(),
                FunctionCode == Modbus.ReadInputs ? "inputs" : "coils",
                Data);
        }

        /// <summary>
        /// 特殊初始化。
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            if (frame.Length < 3 + frame[2])
                throw new FormatException("Message frame data segment does not contain enough bytes.");

            ByteCount = frame[2];
            Data = new DiscreteCollection(frame.Slice(3, ByteCount).ToArray());
        }
    }
}
