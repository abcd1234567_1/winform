namespace Modbus.IO
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    
    using Message;
    
    using Unme.Common;

    /// <summary>
    ///     TCP互联网协议的传输。
    ///      http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    internal class ModbusIpTransport : ModbusTransport
    {
        /// <summary>
        /// 传输ID锁，多线程控制TCP传输ID的访问权限。
        /// </summary>
        private static readonly object _transactionIdLock = new object();
        /// <summary>
        /// 传输ID编号
        /// </summary>
        private ushort _transactionId;

        /// <summary>
        /// 初始化TCP传输实例。
        /// </summary>
        /// <param name="streamResource">数据流资源对象</param>
        internal ModbusIpTransport(IStreamResource streamResource)
            : base(streamResource)
        {
            Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");
        }

        /// <summary>
        /// 读请求、响应帧
        /// </summary>
        /// <param name="streamResource">数据流资源对象</param>
        /// <returns>数据帧数组</returns>
        internal static byte[] ReadRequestResponse(IStreamResource streamResource)
        {
            // 读取帧头
            var mbapHeader = new byte[6];
            int numBytesRead = 0;

            while (numBytesRead != 6)
            {
                int bRead = streamResource.Read(mbapHeader, numBytesRead, 6 - numBytesRead);

                if (bRead == 0)
                    throw new IOException("Read resulted in 0 bytes returned.");

                numBytesRead += bRead;
            }

            Debug.WriteLine("MBAP header: {0}", string.Join(", ", mbapHeader));
            var frameLength = (ushort) IPAddress.HostToNetworkOrder(BitConverter.ToInt16(mbapHeader, 4));
            Debug.WriteLine("{0} bytes in PDU.", frameLength);

            // 读取信息
            var messageFrame = new byte[frameLength];
            numBytesRead = 0;

            while (numBytesRead != frameLength)
            {
                int bRead = streamResource.Read(messageFrame, numBytesRead, frameLength - numBytesRead);

                if (bRead == 0)
                    throw new IOException("Read resulted in 0 bytes returned.");

                numBytesRead += bRead;
            }

            Debug.WriteLine("PDU: {0}", frameLength);
            var frame = mbapHeader.Concat(messageFrame).ToArray();
            Debug.WriteLine("RX: {0}", string.Join(", ", frame));

            return frame;
        }

        /// <summary>
        /// 获取报文头部（MBAP）
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        internal static byte[] GetMbapHeader(IModbusMessage message)
        {
            byte[] transactionId = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short) message.TransactionId));
            byte[] length = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short) (message.ProtocolDataUnit.Length + 1)));

            var stream = new MemoryStream(7);
            stream.Write(transactionId, 0, transactionId.Length);
            stream.WriteByte(0);
            stream.WriteByte(0);
            stream.Write(length, 0, length.Length);
            stream.WriteByte(message.SlaveAddress);

            return stream.ToArray();
        }

        /// <summary>
        ///  创建一个新的传输ID号
        /// </summary>
        internal virtual ushort GetNewTransactionId()
        {
            lock (_transactionIdLock)
                _transactionId = _transactionId == UInt16.MaxValue ? (ushort) 1 : ++_transactionId;

            return _transactionId;
        }

        /// <summary>
        /// 创建一个modbus消息同时创建一个传输控制ID
        /// </summary>
        /// <typeparam name="T">modbus消息</typeparam>
        /// <param name="fullFrame">完整数据帧字节数组</param>
        /// <returns>返回一个响应消息</returns>
        internal IModbusMessage CreateMessageAndInitializeTransactionId<T>(byte[] fullFrame)
            where T : IModbusMessage, new()
        {
            byte[] mbapHeader = fullFrame.Slice(0, 6).ToArray();
            byte[] messageFrame = fullFrame.Slice(6, fullFrame.Length - 6).ToArray();
            // 创建响应消息
            IModbusMessage response = CreateResponse<T>(messageFrame);
            response.TransactionId = (ushort) IPAddress.NetworkToHostOrder(BitConverter.ToInt16(mbapHeader, 0));

            return response;
        }

        /// <summary>
        /// 构建modbus消息，完整的协议包数据。
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        internal override byte[] BuildMessageFrame(IModbusMessage message)
        {
            byte[] header = GetMbapHeader(message);
            byte[] pdu = message.ProtocolDataUnit;
            var messageBody = new MemoryStream(header.Length + pdu.Length);

            messageBody.Write(header, 0, header.Length);
            messageBody.Write(pdu, 0, pdu.Length);

            return messageBody.ToArray();
        }


        /// <summary>
        /// 写入数据，重写父类写方法。
        /// </summary>
        /// <param name="message"></param>
        internal override void Write(IModbusMessage message)
        {
            message.TransactionId = GetNewTransactionId();
            byte[] frame = BuildMessageFrame(message);
            Debug.WriteLine("TX: {0}", string.Join(", ", frame));
            StreamResource.Write(frame, 0, frame.Length);
        }

        /// <summary>
        /// 读取请求消息字节数组
        /// </summary>
        /// <returns>请求数组</returns>
        internal override byte[] ReadRequest()
        {
            return ReadRequestResponse(StreamResource);
        }

        /// <summary>
        /// 读取响应消息。
        /// </summary>
        /// <typeparam name="T">modbus消息类型</typeparam>
        /// <returns>返回响应消息</returns>
        internal override IModbusMessage ReadResponse<T>()
        {
            return CreateMessageAndInitializeTransactionId<T>(ReadRequestResponse(StreamResource));
        }

        /// <summary>
        /// 验证响应的消息。
        /// </summary>
        /// <param name="request">请求消息包</param>
        /// <param name="response">响应消息包</param>
        internal override void OnValidateResponse(IModbusMessage request, IModbusMessage response)
        {
            if (request.TransactionId != response.TransactionId)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Response was not of expected transaction ID. Expected {0}, received {1}.", request.TransactionId,
                    response.TransactionId));
        }

        /// <summary>
        /// 获取是否重新接收响应消息包。
        /// </summary>
        /// <param name="request">请求消息包</param>
        /// <param name="response">响应消息包</param>
        /// <returns>返回bool，true表示需要接收，false表示不需要接收</returns>
        internal override bool OnShouldRetryResponse(IModbusMessage request, IModbusMessage response)
        {
            if (request.TransactionId > response.TransactionId && request.TransactionId - response.TransactionId < RetryOnOldResponseThreshold)
            {
                // 这个响应包是前一个请求产生的
                return true;
            }

            return base.OnShouldRetryResponse(request, response);
        }
    }
}
