﻿namespace Modbus.Data
{
    /// <summary>
    ///  Modbus协议支持的数据类型。
    /// </summary>
    public enum ModbusDataType
    {
        /// <summary>
        /// 可读/写保持寄存器
        /// </summary>
        HoldingRegister,

        /// <summary>
        /// 只读输入寄存器
        /// </summary>
        InputRegister,

        /// <summary>
        ///  可读/写离散线圈
        /// </summary>
        Coil,

        /// <summary>
        /// 只读离散输入
        /// </summary>
        Input
    }
}
