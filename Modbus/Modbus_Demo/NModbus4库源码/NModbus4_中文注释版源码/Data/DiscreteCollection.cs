namespace Modbus.Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;

    /// <summary>
    ///  离散值的集合。包括线圈和离散输入数据。
    /// </summary>
    public class DiscreteCollection : Collection<bool>, IModbusMessageDataCollection
    {
        /// <summary>
        /// 字节位数 默认8位。
        /// </summary>
        private const int BitsPerByte = 8;
        /// <summary>
        /// bool型离散数据列表。
        /// </summary>
        private readonly List<bool> _discretes;

        /// <summary>
        ///  创建对象的新实例 <see cref="DiscreteCollection" />.
        /// </summary>
        public DiscreteCollection()
            : this(new List<bool>())
        {
        }

        /// <summary>
        ///    使用bool数组创建对象的新实例 <see cref="DiscreteCollection" />.
        /// </summary>
        /// <param name="bits">bool数组</param>
        public DiscreteCollection(params bool[] bits)
            : this((IList<bool>)bits)
        {
        }

        /// <summary>
        ///   使用字节数组创建对象的新实例 <see cref="DiscreteCollection" />.
        /// </summary>
        /// <param name="bytes">字节数组</param>
        public DiscreteCollection(params byte[] bytes)
            : this()
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            _discretes.Capacity = bytes.Length * BitsPerByte;
            foreach (byte b in bytes)
            {
                _discretes.Add((b & 1) == 1);
                _discretes.Add((b & 2) == 2);
                _discretes.Add((b & 4) == 4);
                _discretes.Add((b & 8) == 8);
                _discretes.Add((b & 16) == 16);
                _discretes.Add((b & 32) == 32);
                _discretes.Add((b & 64) == 64);
                _discretes.Add((b & 128) == 128);
            }
        }

        /// <summary>
        ///    使用bool列表创建对象的新实例 <see cref="DiscreteCollection" />.
        /// </summary>
        /// <param name="bits">bool列表</param>
        public DiscreteCollection(IList<bool> bits)
            : this(new List<bool>(bits))
        {
        }

        /// <summary>
        ///    使用bool列表创建对象的新实例 <see cref="DiscreteCollection" />.
        /// </summary>
        /// <param name="bits">bool列表对象</param>
        internal DiscreteCollection(List<bool> bits)
            : base(bits)
        {
            Debug.Assert(bits != null);
            _discretes = bits;
        }

        /// <summary>
        ///  获取网络字节。
        /// </summary>
        public byte[] NetworkBytes
        {
            get
            {
                byte[] bytes = new byte[ByteCount];

                for (int index = 0; index < _discretes.Count; index++)
                {
                    if (_discretes[index])
                    {
                        bytes[index / BitsPerByte] |= (byte)(1 << (index % BitsPerByte));
                    }
                }

                return bytes;
            }
        }

        /// <summary>
        ///  获取字节计数。
        /// </summary>
        public byte ByteCount
        {
            get { return (byte) ((Count + 7)/8); }
        }

        /// <summary>
        ///  当前对象转字符串
        /// </summary>
        /// <returns>
        ///  对象字符串
        /// </returns>
        public override string ToString()
        {
            return String.Concat("{", String.Join(", ", this.Select(discrete => discrete ? "1" : "0").ToArray()), "}");
        }
    }
}
