﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modbus
{
    public class ModbusTransportEventArgs : EventArgs
    {
        private byte[] _message;

        internal ModbusTransportEventArgs(byte[] message)
        {
            _message = message;
        }

        //语法糖:只读属性,相当于下面注释
        public byte[] Message => _message;

        //public byte[] Message { get => _message; }

        //public byte[] Message
        //{
        //    get { return _message; }
        //}

        //读写属性
        //public byte[] Message
        //{
        //    get => _message;
        //    set => _message = value;
        //}

    }
}
