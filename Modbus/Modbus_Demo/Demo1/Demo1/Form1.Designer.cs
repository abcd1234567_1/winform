﻿
namespace Demo1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.connectType_cbb = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ASCII_rbtn = new System.Windows.Forms.RadioButton();
            this.RTU_rbtn = new System.Windows.Forms.RadioButton();
            this.serialRTS_chb = new System.Windows.Forms.CheckBox();
            this.serialCTS_chb = new System.Windows.Forms.CheckBox();
            this.serialDSR_chb = new System.Windows.Forms.CheckBox();
            this.serialStopbit_cbb = new System.Windows.Forms.ComboBox();
            this.serialParity_cbb = new System.Windows.Forms.ComboBox();
            this.serialDatabit_cbb = new System.Windows.Forms.ComboBox();
            this.serialBaud_cbb = new System.Windows.Forms.ComboBox();
            this.serialPort_cbb = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.connect_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.state_tssl = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.recivecmd_rtb = new System.Windows.Forms.RichTextBox();
            this.sendcmd_rtb = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.write_btn = new System.Windows.Forms.Button();
            this.read_btn = new System.Windows.Forms.Button();
            this.data_txb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.num_txb = new System.Windows.Forms.TextBox();
            this.startAddress_txb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.option_cbb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.station_txb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.connectType_cbb);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(245, 50);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "连接类型";
            // 
            // connectType_cbb
            // 
            this.connectType_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.connectType_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.connectType_cbb.FormattingEnabled = true;
            this.connectType_cbb.Location = new System.Drawing.Point(6, 20);
            this.connectType_cbb.Name = "connectType_cbb";
            this.connectType_cbb.Size = new System.Drawing.Size(233, 20);
            this.connectType_cbb.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ASCII_rbtn);
            this.groupBox2.Controls.Add(this.RTU_rbtn);
            this.groupBox2.Controls.Add(this.serialRTS_chb);
            this.groupBox2.Controls.Add(this.serialCTS_chb);
            this.groupBox2.Controls.Add(this.serialDSR_chb);
            this.groupBox2.Controls.Add(this.serialStopbit_cbb);
            this.groupBox2.Controls.Add(this.serialParity_cbb);
            this.groupBox2.Controls.Add(this.serialDatabit_cbb);
            this.groupBox2.Controls.Add(this.serialBaud_cbb);
            this.groupBox2.Controls.Add(this.serialPort_cbb);
            this.groupBox2.Location = new System.Drawing.Point(12, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(245, 164);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "串口配置";
            // 
            // ASCII_rbtn
            // 
            this.ASCII_rbtn.AutoSize = true;
            this.ASCII_rbtn.Location = new System.Drawing.Point(174, 127);
            this.ASCII_rbtn.Name = "ASCII_rbtn";
            this.ASCII_rbtn.Size = new System.Drawing.Size(53, 16);
            this.ASCII_rbtn.TabIndex = 3;
            this.ASCII_rbtn.Text = "ASCII";
            this.ASCII_rbtn.UseVisualStyleBackColor = true;
            this.ASCII_rbtn.CheckedChanged += new System.EventHandler(this.ASCII_rbtn_CheckedChanged);
            // 
            // RTU_rbtn
            // 
            this.RTU_rbtn.AutoSize = true;
            this.RTU_rbtn.Checked = true;
            this.RTU_rbtn.Location = new System.Drawing.Point(114, 127);
            this.RTU_rbtn.Name = "RTU_rbtn";
            this.RTU_rbtn.Size = new System.Drawing.Size(41, 16);
            this.RTU_rbtn.TabIndex = 2;
            this.RTU_rbtn.TabStop = true;
            this.RTU_rbtn.Text = "RTU";
            this.RTU_rbtn.UseVisualStyleBackColor = true;
            this.RTU_rbtn.CheckedChanged += new System.EventHandler(this.RTU_rbtn_CheckedChanged);
            // 
            // serialRTS_chb
            // 
            this.serialRTS_chb.AutoSize = true;
            this.serialRTS_chb.Location = new System.Drawing.Point(114, 72);
            this.serialRTS_chb.Name = "serialRTS_chb";
            this.serialRTS_chb.Size = new System.Drawing.Size(42, 16);
            this.serialRTS_chb.TabIndex = 3;
            this.serialRTS_chb.Text = "RTS";
            this.serialRTS_chb.UseVisualStyleBackColor = true;
            // 
            // serialCTS_chb
            // 
            this.serialCTS_chb.AutoSize = true;
            this.serialCTS_chb.Location = new System.Drawing.Point(174, 50);
            this.serialCTS_chb.Name = "serialCTS_chb";
            this.serialCTS_chb.Size = new System.Drawing.Size(42, 16);
            this.serialCTS_chb.TabIndex = 3;
            this.serialCTS_chb.Text = "CTS";
            this.serialCTS_chb.UseVisualStyleBackColor = true;
            // 
            // serialDSR_chb
            // 
            this.serialDSR_chb.AutoSize = true;
            this.serialDSR_chb.Location = new System.Drawing.Point(114, 50);
            this.serialDSR_chb.Name = "serialDSR_chb";
            this.serialDSR_chb.Size = new System.Drawing.Size(42, 16);
            this.serialDSR_chb.TabIndex = 2;
            this.serialDSR_chb.Text = "DSR";
            this.serialDSR_chb.UseVisualStyleBackColor = true;
            // 
            // serialStopbit_cbb
            // 
            this.serialStopbit_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.serialStopbit_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialStopbit_cbb.FormattingEnabled = true;
            this.serialStopbit_cbb.Location = new System.Drawing.Point(6, 124);
            this.serialStopbit_cbb.Name = "serialStopbit_cbb";
            this.serialStopbit_cbb.Size = new System.Drawing.Size(82, 20);
            this.serialStopbit_cbb.TabIndex = 4;
            // 
            // serialParity_cbb
            // 
            this.serialParity_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.serialParity_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialParity_cbb.FormattingEnabled = true;
            this.serialParity_cbb.Location = new System.Drawing.Point(7, 98);
            this.serialParity_cbb.Name = "serialParity_cbb";
            this.serialParity_cbb.Size = new System.Drawing.Size(82, 20);
            this.serialParity_cbb.TabIndex = 4;
            // 
            // serialDatabit_cbb
            // 
            this.serialDatabit_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.serialDatabit_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialDatabit_cbb.FormattingEnabled = true;
            this.serialDatabit_cbb.Location = new System.Drawing.Point(7, 72);
            this.serialDatabit_cbb.Name = "serialDatabit_cbb";
            this.serialDatabit_cbb.Size = new System.Drawing.Size(82, 20);
            this.serialDatabit_cbb.TabIndex = 4;
            // 
            // serialBaud_cbb
            // 
            this.serialBaud_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.serialBaud_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialBaud_cbb.FormattingEnabled = true;
            this.serialBaud_cbb.Location = new System.Drawing.Point(6, 46);
            this.serialBaud_cbb.Name = "serialBaud_cbb";
            this.serialBaud_cbb.Size = new System.Drawing.Size(82, 20);
            this.serialBaud_cbb.TabIndex = 3;
            // 
            // serialPort_cbb
            // 
            this.serialPort_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.serialPort_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serialPort_cbb.FormattingEnabled = true;
            this.serialPort_cbb.Location = new System.Drawing.Point(6, 20);
            this.serialPort_cbb.Name = "serialPort_cbb";
            this.serialPort_cbb.Size = new System.Drawing.Size(233, 20);
            this.serialPort_cbb.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(12, 238);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(245, 138);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "网络配置";
            // 
            // connect_btn
            // 
            this.connect_btn.Location = new System.Drawing.Point(18, 382);
            this.connect_btn.Name = "connect_btn";
            this.connect_btn.Size = new System.Drawing.Size(75, 23);
            this.connect_btn.TabIndex = 3;
            this.connect_btn.Text = "连接";
            this.connect_btn.UseVisualStyleBackColor = true;
            this.connect_btn.Click += new System.EventHandler(this.connect_btn_Click);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(176, 382);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.cancel_btn.TabIndex = 4;
            this.cancel_btn.Text = "取消";
            this.cancel_btn.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.state_tssl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 440);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(682, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "状态:";
            // 
            // state_tssl
            // 
            this.state_tssl.AutoSize = false;
            this.state_tssl.Name = "state_tssl";
            this.state_tssl.Size = new System.Drawing.Size(200, 17);
            this.state_tssl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.recivecmd_rtb);
            this.groupBox4.Controls.Add(this.sendcmd_rtb);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.write_btn);
            this.groupBox4.Controls.Add(this.read_btn);
            this.groupBox4.Controls.Add(this.data_txb);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.num_txb);
            this.groupBox4.Controls.Add(this.startAddress_txb);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.option_cbb);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.station_txb);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(263, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(403, 400);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "基本操作";
            // 
            // recivecmd_rtb
            // 
            this.recivecmd_rtb.Location = new System.Drawing.Point(13, 322);
            this.recivecmd_rtb.Name = "recivecmd_rtb";
            this.recivecmd_rtb.Size = new System.Drawing.Size(358, 68);
            this.recivecmd_rtb.TabIndex = 23;
            this.recivecmd_rtb.Text = "";
            // 
            // sendcmd_rtb
            // 
            this.sendcmd_rtb.Location = new System.Drawing.Point(13, 231);
            this.sendcmd_rtb.Name = "sendcmd_rtb";
            this.sendcmd_rtb.Size = new System.Drawing.Size(358, 68);
            this.sendcmd_rtb.TabIndex = 22;
            this.sendcmd_rtb.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 307);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "应答指令:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 19;
            this.label6.Text = "发出指令:";
            // 
            // write_btn
            // 
            this.write_btn.Location = new System.Drawing.Point(102, 184);
            this.write_btn.Name = "write_btn";
            this.write_btn.Size = new System.Drawing.Size(75, 23);
            this.write_btn.TabIndex = 18;
            this.write_btn.Text = "写入";
            this.write_btn.UseVisualStyleBackColor = true;
            this.write_btn.Click += new System.EventHandler(this.write_btn_Click);
            // 
            // read_btn
            // 
            this.read_btn.Location = new System.Drawing.Point(14, 184);
            this.read_btn.Name = "read_btn";
            this.read_btn.Size = new System.Drawing.Size(75, 23);
            this.read_btn.TabIndex = 17;
            this.read_btn.Text = "读取";
            this.read_btn.UseVisualStyleBackColor = true;
            this.read_btn.Click += new System.EventHandler(this.read_btn_Click);
            // 
            // data_txb
            // 
            this.data_txb.Location = new System.Drawing.Point(50, 97);
            this.data_txb.Multiline = true;
            this.data_txb.Name = "data_txb";
            this.data_txb.Size = new System.Drawing.Size(321, 60);
            this.data_txb.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "数据:";
            // 
            // num_txb
            // 
            this.num_txb.Location = new System.Drawing.Point(217, 55);
            this.num_txb.Name = "num_txb";
            this.num_txb.Size = new System.Drawing.Size(86, 21);
            this.num_txb.TabIndex = 14;
            this.num_txb.Text = "1";
            // 
            // startAddress_txb
            // 
            this.startAddress_txb.Location = new System.Drawing.Point(82, 54);
            this.startAddress_txb.Name = "startAddress_txb";
            this.startAddress_txb.Size = new System.Drawing.Size(80, 21);
            this.startAddress_txb.TabIndex = 13;
            this.startAddress_txb.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(175, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "数量:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "起始地址:";
            // 
            // option_cbb
            // 
            this.option_cbb.BackColor = System.Drawing.SystemColors.Window;
            this.option_cbb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.option_cbb.FormattingEnabled = true;
            this.option_cbb.Location = new System.Drawing.Point(141, 20);
            this.option_cbb.Name = "option_cbb";
            this.option_cbb.Size = new System.Drawing.Size(233, 20);
            this.option_cbb.TabIndex = 10;
            this.option_cbb.SelectedIndexChanged += new System.EventHandler(this.option_cbb_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "操作:";
            // 
            // station_txb
            // 
            this.station_txb.Location = new System.Drawing.Point(52, 19);
            this.station_txb.Name = "station_txb";
            this.station_txb.Size = new System.Drawing.Size(32, 21);
            this.station_txb.TabIndex = 8;
            this.station_txb.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "站号:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "数据信息请使用英文逗号分开";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 462);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.connect_btn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "ModbusHelper";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox connectType_cbb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox serialPort_cbb;
        private System.Windows.Forms.ComboBox serialStopbit_cbb;
        private System.Windows.Forms.ComboBox serialParity_cbb;
        private System.Windows.Forms.ComboBox serialDatabit_cbb;
        private System.Windows.Forms.ComboBox serialBaud_cbb;
        private System.Windows.Forms.CheckBox serialDSR_chb;
        private System.Windows.Forms.CheckBox serialCTS_chb;
        private System.Windows.Forms.CheckBox serialRTS_chb;
        private System.Windows.Forms.RadioButton RTU_rbtn;
        private System.Windows.Forms.RadioButton ASCII_rbtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button connect_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox station_txb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox option_cbb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox num_txb;
        private System.Windows.Forms.TextBox startAddress_txb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox data_txb;
        private System.Windows.Forms.Button read_btn;
        private System.Windows.Forms.Button write_btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox sendcmd_rtb;
        private System.Windows.Forms.RichTextBox recivecmd_rtb;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel state_tssl;
        private System.Windows.Forms.Label label8;
    }
}

