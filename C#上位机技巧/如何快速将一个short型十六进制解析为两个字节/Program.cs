﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 如何快速将一个short型十六进制解析为两个字节
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 需求:一个short类型的十六进制需要解析出两个字节，能否举例说明使用C#如何解析？
            short value = 0x2B7E;
            // value的二进制
            Console.WriteLine($"value的二进制数据:{Convert.ToString(value, 2).PadLeft(16, '0')}"); //二进制数: 00101011 01111110
            // 右移8位 得到高字节
            byte hightByte = (byte)(value >> 8); //  00101011 01111110 >> 8 =  00000000 00101011 强制转换byte后取右边8位,得到00101011
            // 和0XFF做&操作 得到低字节
            byte lowByte = (byte)(value & 0xFF);
            //  00101011 01111110
            //& 00000000 11111111
            //========================
            //  00000000 01111110   
            // 强制转换byte后取右边8位,得到01111110


            Console.WriteLine($"hightByte的十六进制数据:{Convert.ToString(hightByte, 16).ToUpper()}");
            Console.WriteLine($"lowByte的十六进制数据:{lowByte.ToString("X2")}");

            Console.WriteLine($"hightByte的十进制数据:{hightByte}");
            Console.WriteLine($"lowByte的十进制数据:{lowByte}");

            // .PadLeft(8,'0'):未满足8位填充0
            Console.WriteLine($"hightByte的二进制数据:{Convert.ToString(hightByte, 2).PadLeft(8, '0')}");
            Console.WriteLine($"lowByte的二进制数据:{Convert.ToString(lowByte, 2).PadLeft(8, '0')}");

            Console.ReadKey();
        }
    }
}
