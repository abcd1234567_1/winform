﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgressBarDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void output(string info)
        {
            txtMsg.Clear();
            txtMsg.AppendText(DateTime.Now.ToString("HH:mm:ss") + info + "\r\n");
        }

        /// <summary>
        /// 进度条开始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtLenth.Text))
            {
                MessageBox.Show("请输入进度条长度", "警告");
                return;
            }
            else
            {
                progressBar1.Value = 0;
                progressBar1.Minimum = 0;
                progressBar1.Maximum = int.Parse(txtLenth.Text);
                output("进度条开始");
                timer1.Enabled = true;
            }
        }

        /// <summary>
        /// 进度条暂停
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPause_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
            {
                output("进度条暂停");
                btnPause.Text = "继续";
                timer1.Enabled = false;
            }
            else
            {
                output("进度条继续");
                btnPause.Text = "暂停";
                timer1.Enabled = true;
            }
        }

        /// <summary>
        /// 结束进度条
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, EventArgs e)
        {
            output("进度条停止");
            progressBar1.Value = 0;
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < progressBar1.Maximum)
            {
                progressBar1.Value++; // 进度值自增
                output("进度运行中(" + progressBar1.Value.ToString() + "/" + progressBar1.Maximum + ")...");
            }
            else
            {
                output("进度完成");
                timer1.Enabled = false;
            }
        }


    }
}
