﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 二进制_八进制_十进制_十六进制之间相互转换
{
    /// <summary>
    /// 介绍C#中二进制、八进制、十进制、十六进制数值之间的相互转换，使用Convert.ToString方法以及Convert.ToInt32方法
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * int a = 0xFF;    十六进制，a=255
               int b = 0B101;   二进制，b=5
               16进制前缀   0x
               10进制      0d 一般不用写
               2进制        0b
             * 
             * 
             * 
             */

            //MyTest();

            int num2 = 0B1110;
            int num16 = 0XA;

            //int num2 = 0b1110;
            //int num16 = 0xA;

            //Console.WriteLine(num2);  // 打印的结果是转为10进制的14
            //Console.WriteLine(num16); // 打印的结果是转为10进制的10

            //// 二进制转换为16进制
            //string num2to16 = Convert.ToString(num2, 16);
            //Console.WriteLine(num2to16); // 输出16进制:e

            //// 16进制转换为2进制
            //string num16to2 = Convert.ToString(num16, 2);
            //Console.WriteLine(num16to2); // 输出1010

            ////二进制转换成8进制
            //string num2to8 = Convert.ToString(num2, 8);
            //Console.WriteLine(num2to8); // 输出八进制16

            ////16进制转换成8进制
            //string num16to8 = Convert.ToString(num16, 8);
            //Console.WriteLine(num16to8); // 输出八进制12

            //// 八进制在C#中没有具体的表示，可以使用 int v=Convert.ToInt32("10", 8); // 10代表你要转化的8进制数，v=十进制的8
            //// 8进制转换成其他进制，先要转换为10进制的8进制的值
            //int num8to10 = Convert.ToInt32("100", 8);
            //Console.WriteLine(num8to10); // 十进制为64
            //// 然后再转为其他进制,比如这里是转为16进制
            //string num8to10to16 = Convert.ToString(num8to10, 16);
            //Console.WriteLine(num8to10to16); // 输出16进制的40

            //2进制转10进制
            //string num2to10=Convert.ToString(num2, 10);
            //Console.WriteLine(num2to10); // 输出10进制的14
            //// 将1110二进制转换为10进制
            //int a=Convert.ToInt32("1110", 2);
            //Console.WriteLine(a); // 输出10进制的14

            //10进制数转换成2进制数
            //string num10to2 = Convert.ToString(14, 2);
            //Console.WriteLine(num10to2);// 输出2进制的1110

            //16进制转成成10进制
            //string num16to10 = Convert.ToString(num16, 10);
            //Console.WriteLine(num16to10); // 输出10进制的10
            //// 将"a"16进制转换为10进制
            //int b =Convert.ToInt32("a", 16);
            //Console.WriteLine(b);// 输出10进制的10

            // 10进制转换为16进制
            string num10to16 = Convert.ToString(10, 16);
            Console.WriteLine(num10to16); // a


            Console.ReadKey();
        }

        /// <summary>
        /// 自己的测试
        /// </summary>
        private static void MyTest()
        {
            string val = "17";

            // 这里假设是16进制的17转为10进制,结果为:23
            // 同理转其他进制如2,8,10进制,只需要改第二个参数为2,8,10即可
            // 比如2进制转10进制,你的字符串必须是"1010","0101"等
            // 比如8进制转10进制,你的字符串必须是"12","16"等
            // 比如16进制转10进制,你的字符串必须是"EF","FF"等
            // Convert.ToInt32：的第二个参数代表:以什么进制来转换为10进制
            int result = Convert.ToInt32(val, 16);
            Console.WriteLine(result); // 输出结果为23

            // 那么如果我希望是以16进制的形式字符串输出
            // Convert.ToString：代表转为什么进制的字符串，参数可以是2,8,10,16
            Console.WriteLine("0x" + Convert.ToString(result, 16)); // 16进制字符串输出则为16进制的17

            string val2 = "00000010";

            int aaa = Convert.ToInt32(val2, 2); // 结果为10进制的2

            Console.WriteLine("0B" + Convert.ToString(aaa, 2));
        }
    }
}
