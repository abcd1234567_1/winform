﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 进制转换器
{
    public partial class Form1 : Form
    {
        // 第一次初始化为10进制
        private int oldHex = 10;

        public Form1()
        {
            InitializeComponent();
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            //获取当前选择的进制
            int newHex = Convert.ToInt32(rb.Tag);
            if (txtEntryCalc.Text != "")
            {
               
                int oldNum = Convert.ToInt32(txtEntryCalc.Text, oldHex);

                txtEntryCalc.Text =Convert.ToString(oldNum,newHex);
            }
            oldHex = newHex;
        }


    }
}
